package eu.slachta.pdp.controller;

import java.security.Principal;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ResultsController {
	
	@RequestMapping(value="/results", method = RequestMethod.GET)
	public String printTypesOfTests(ModelMap model, Principal principal) {
		return "results";
	}
}
