package eu.slachta.pdp.model.server;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the bourdonnorm database table.
 * 
 */
@Entity
@Table(name="bourdonnorm")
@NamedQuery(name="Bourdonnorm.findAll", query="SELECT b FROM Bourdonnorm b")
public class Bourdonnorm implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int id;

	@Column(length=45)
	private String description;

	@Column(nullable=false)
	private double iqr;

	@Column(nullable=false)
	private double lowerQuartile;

	@Column(nullable=false)
	private double maximum;

	@Column(nullable=false)
	private double mean;

	@Column(nullable=false)
	private double median;

	@Column(nullable=false)
	private double minimum;

	@Column(unique=true, nullable=false, length=45)
	private String name;

	@Column(nullable=false)
	private int numberOfSamples;

	@Column(nullable=false)
	private double upperQuartile;

	public Bourdonnorm() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getIqr() {
		return this.iqr;
	}

	public void setIqr(double iqr) {
		this.iqr = iqr;
	}

	public double getLowerQuartile() {
		return this.lowerQuartile;
	}

	public void setLowerQuartile(double lowerQuartile) {
		this.lowerQuartile = lowerQuartile;
	}

	public double getMaximum() {
		return this.maximum;
	}

	public void setMaximum(double maximum) {
		this.maximum = maximum;
	}

	public double getMean() {
		return this.mean;
	}

	public void setMean(double mean) {
		this.mean = mean;
	}

	public double getMedian() {
		return this.median;
	}

	public void setMedian(double median) {
		this.median = median;
	}

	public double getMinimum() {
		return this.minimum;
	}

	public void setMinimum(double minimum) {
		this.minimum = minimum;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumberOfSamples() {
		return this.numberOfSamples;
	}

	public void setNumberOfSamples(int numberOfSamples) {
		this.numberOfSamples = numberOfSamples;
	}

	public double getUpperQuartile() {
		return this.upperQuartile;
	}

	public void setUpperQuartile(double upperQuartile) {
		this.upperQuartile = upperQuartile;
	}

}