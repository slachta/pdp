package eu.slachta.pdp.model.server;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the bourdontest database table.
 * 
 */
@Entity
@Table(name="bourdontest")
@NamedQuery(name="Bourdontest.findAll", query="SELECT b FROM Bourdontest b")
public class Bourdontest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int id;

	@Column(nullable=false, length=42)
	private String apiKey;

	@Column(nullable=false)
	private int cellSize;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	private Date date;

	@Column(nullable=false)
	private int dpi;

	@Column(nullable=false)
	private int imagesPerPage;

	@Column(nullable=false, length=45)
	private String note;

	@Column(nullable=false)
	private int orientation;

	@Column(nullable=false)
	private int pages;

	@Column(nullable=false)
	private int refImg1Id;

	@Column(nullable=false)
	private int refImg2Id;

	@Column(nullable=false)
	private int refImg3Id;

	@Column(nullable=false, length=45)
	private String resolution;

	@Column(nullable=false)
	private int timePerPage;

	//bi-directional many-to-one association to Respondent
	@ManyToOne
	@JoinColumn(name="respondentId", nullable=false)
	private Respondent respondent;

	//bi-directional many-to-one association to Testtype
	@ManyToOne
	@JoinColumn(name="testtype", nullable=false)
	private Testtype testtype;

	//bi-directional many-to-one association to Bourdontestpage
	@OneToMany(mappedBy="bourdontest")
	private List<Bourdontestpage> bourdontestpages;

	public Bourdontest() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getApiKey() {
		return this.apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public int getCellSize() {
		return this.cellSize;
	}

	public void setCellSize(int cellSize) {
		this.cellSize = cellSize;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getDpi() {
		return this.dpi;
	}

	public void setDpi(int dpi) {
		this.dpi = dpi;
	}

	public int getImagesPerPage() {
		return this.imagesPerPage;
	}

	public void setImagesPerPage(int imagesPerPage) {
		this.imagesPerPage = imagesPerPage;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public int getOrientation() {
		return this.orientation;
	}

	public void setOrientation(int orientation) {
		this.orientation = orientation;
	}

	public int getPages() {
		return this.pages;
	}

	public void setPages(int pages) {
		this.pages = pages;
	}

	public int getRefImg1Id() {
		return this.refImg1Id;
	}

	public void setRefImg1Id(int refImg1Id) {
		this.refImg1Id = refImg1Id;
	}

	public int getRefImg2Id() {
		return this.refImg2Id;
	}

	public void setRefImg2Id(int refImg2Id) {
		this.refImg2Id = refImg2Id;
	}

	public int getRefImg3Id() {
		return this.refImg3Id;
	}

	public void setRefImg3Id(int refImg3Id) {
		this.refImg3Id = refImg3Id;
	}

	public String getResolution() {
		return this.resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}

	public int getTimePerPage() {
		return this.timePerPage;
	}

	public void setTimePerPage(int timePerPage) {
		this.timePerPage = timePerPage;
	}

	public Respondent getRespondent() {
		return this.respondent;
	}

	public void setRespondent(Respondent respondent) {
		this.respondent = respondent;
	}

	public Testtype getTesttype() {
		return this.testtype;
	}

	public void setTesttype(Testtype testtype) {
		this.testtype = testtype;
	}

	public List<Bourdontestpage> getBourdontestpages() {
		return this.bourdontestpages;
	}

	public void setBourdontestpages(List<Bourdontestpage> bourdontestpages) {
		this.bourdontestpages = bourdontestpages;
	}

	public Bourdontestpage addBourdontestpage(Bourdontestpage bourdontestpage) {
		getBourdontestpages().add(bourdontestpage);
		bourdontestpage.setBourdontest(this);

		return bourdontestpage;
	}

	public Bourdontestpage removeBourdontestpage(Bourdontestpage bourdontestpage) {
		getBourdontestpages().remove(bourdontestpage);
		bourdontestpage.setBourdontest(null);

		return bourdontestpage;
	}

}