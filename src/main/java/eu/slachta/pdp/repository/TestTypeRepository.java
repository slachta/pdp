package eu.slachta.pdp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.slachta.pdp.model.server.Testtype;

public interface TestTypeRepository extends JpaRepository<Testtype, Integer> {
	
}
