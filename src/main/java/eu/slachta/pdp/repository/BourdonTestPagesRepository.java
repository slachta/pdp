package eu.slachta.pdp.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import eu.slachta.pdp.model.server.Bourdontestpage;

public interface BourdonTestPagesRepository extends JpaRepository<Bourdontestpage, Integer> {
	@Query("SELECT b FROM Bourdontestpage b WHERE b.bourdontest.id=:testId ")
	public ArrayList<Bourdontestpage> findByTestId(@Param(value = "testId") Integer testId);
}
