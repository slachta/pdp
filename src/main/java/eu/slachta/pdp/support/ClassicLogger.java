package eu.slachta.pdp.support;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClassicLogger implements Support{
	private final Class<?> instance;
	private Logger logger;
	
	public ClassicLogger(Class<?> clazz){
		this.instance = clazz;
		logger = LoggerFactory.getLogger(clazz);
	}

	@Override
	public void logFormManipulation(String errorInfo) {
		logger.warn("HTML form manipulation detected: " + errorInfo);
	}

	@Override
	public void logDateIntegrityViolation(String action, String usedValue,
			String infoText) {
		logger.error("Following action caused DIVE: " + action + " use value: " + usedValue + ". Additional information: " + infoText);
	}

	@Override
	public void logUserLogin(String username, String role, String firstTime) {
		if (logger.isInfoEnabled()){
			logger.info("Login detected " + username + " with grantedRole " + role + " firstTime " + firstTime);
		}
	}

	@Override
	public void logUnauthorizedAccessAttempt() {
		logger.warn("Unauthorized access attempt detected!");
	}

	@Override
	public void logoutInfo(String username) {
		if (logger.isInfoEnabled()){
			logger.info("Logout of user detected: " + username);
		}
	}

	@Override
	public void logDatabaseTransaction(String action, String id) {
		if (logger.isInfoEnabled()){
			logger.info("DB action: " + action + " with value " + id);
		}
	}

	@Override
	public void logDatabaseTransactionError(String action, Throwable cause) {
		logger.warn("Error while database transaction on " + action + " cause: " + cause.getMessage());
	}

	@Override
	public void logLoginWithNoOrganization(String username) {
		logger.error("Authentication failed, no organization specified for username eppn: " + username);
	}

	public Class<?> getInstance() {
		return instance;
	}
}
