<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8" language="java"
	contentType="text/html; charset=UTF-8"%>

<c:url var="postLoginUrl" value="/j_spring_security_check" />

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.ico">

<title>Psychodiagnostika pozornosti - přihlášení</title>

<!-- Bootstrap core CSS -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="resources/custom-css/signin.css" rel="stylesheet">

</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-md-4 col-md-offset-4">
				<div class="page-header">
					<h1>
						<small>Přihlaste se do aplikace </small>Psychodiagnostika
						pozornosti
					</h1>
				</div>
				<c:if test="${not empty authfailed}">
					<div class="alert alert-danger"><p>Nastalo neštěstí! Přihlašovací údaje nejsou platné!</p></div>
				</c:if>
				
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Přihlašovací údaje</h3>
					</div>

					<div class="panel-body">
						<form class="form-signin" name='f'
							action="${postLoginUrl}" method='POST'>
								
								<input type="email" name='j_username' class="form-control"
								placeholder="Emailová adresa" required autofocus> 
								
								<input type="password" name='j_password' class="form-control"
								placeholder="Heslo" required> 
								
							<button class="btn btn-lg btn-primary btn-block" type="submit">Přihlásit
								se</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /container -->
	<!-- Bootstrap core JavaScript
			================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="resources/js/jquery-1.11.0.min.js"></script>
	<script src="resources/js/bootstrap.min.js"></script>
</body>
</html>