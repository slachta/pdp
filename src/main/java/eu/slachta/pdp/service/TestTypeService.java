package eu.slachta.pdp.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import eu.slachta.pdp.model.server.Testtype;
import eu.slachta.pdp.repository.TestTypeRepository;

@Service
@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
public class TestTypeService {

	@Resource
	TestTypeRepository testTypeDao;

	public void createTestType(Testtype type) {
		testTypeDao.save(type);
	}

	public void deleteTestType(int testId) {
		testTypeDao.delete(testId);
	}

	public Testtype findById(int testId) {
		return testTypeDao.findOne(testId);
	}

	public List<Testtype> findAll() {
		return testTypeDao.findAll();
	}
}
