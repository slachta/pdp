package eu.slachta.pdp.repository;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import eu.slachta.pdp.model.server.Bourdontest;

public interface BourdonTestRepository extends JpaRepository<Bourdontest, Integer> {
	
	@Query("SELECT b FROM Bourdontest b WHERE b.apiKey=:apiKey ")
	public ArrayList<Bourdontest> findTestByApiKey(@Param("apiKey") String apiKey);
	
	@Query("SELECT b FROM Bourdontest b WHERE b.apiKey=:apiKey AND b.date=:date AND b.respondent.id=:respondentId ")
	public ArrayList<Bourdontest> findTestByApiKeyDateAndRespondentId(@Param(value = "apiKey") String apiKey, @Param(value = "date") Date date, @Param(value = "respondentId") Integer respondentId);
	
	@Query("SELECT b FROM Bourdontest b WHERE b.apiKey=:apiKey AND b.respondent.id=:respondentId ")
	public ArrayList<Bourdontest> findTestByApiKeyAndRespondentId(@Param(value = "apiKey") String apiKey, @Param(value = "respondentId") Integer respondentId);
	
	@Query("SELECT b FROM Bourdontest b WHERE b.apiKey=:apiKey AND b.date=:date ")
	public ArrayList<Bourdontest> findTestByApiKeyAndDate(@Param(value = "apiKey") String apiKey, @Param(value = "date") Date date);
	
	@Query("SELECT b FROM Bourdontest b WHERE b.respondent.id=:respondentId ")
	public ArrayList<Bourdontest> findTestByRespondentId(@Param(value = "respondentId") Integer respondentId);

	@Query("SELECT b FROM Bourdontest b ORDER BY b.date DESC")
	public ArrayList<Bourdontest> findLastTests(Pageable page);
	
	@Query("SELECT COUNT(b) FROM Bourdontest b WHERE b.respondent.id=:respondentId ")
	public Long existsByRespondentId(@Param(value = "respondentId") Integer respondentId);
}
