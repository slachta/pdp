<%@ page pageEncoding="UTF-8" language="java"
	contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/layout/header.jsp"%>

<div class="panel panel-default">
	<!-- Default panel contents -->
	<div class="panel-heading">Přidat respondenta</div>
	<div class="panel-body">
		<c:if test="${not empty exists}">
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				Respondent s tímto identifikátorem již existuje.
			</div>
		</c:if>
		<c:if test="${not empty notValidUserKey}">
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				Použitý identifikátor respondenta nelze použít. Musí být ve tvaru XXXX-XXXX-XXXX-XXXX, kde v roli X může být písmeno nebo číslo.
			</div>
		</c:if>
		<form:form action="${pageContext.request.contextPath}/respondent/add"
			method="POST" commandName="formData">

			<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label">Jméno:</label>
				<div class="col-sm-10">
					<form:input path="name" cssErrorClass="error" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label">Příjmení:</label>
				<div class="col-sm-10">
					<form:input path="surname" cssErrorClass="error"
						class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label">Věk:</label>
				<div class="col-sm-10">
					<form:input path="age" cssErrorClass="error" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label">Vzdělání:</label>
				<div class="col-sm-10">
					<form:select path="educationLevel" items="${educationLevel}"
						class="form-control"></form:select>
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label">Pohlaví:</label>
				<div class="col-sm-10">
					<form:select path="sex" items="${sex}" class="form-control"></form:select>
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label">Identifikátor:</label>
				<div class="col-sm-10">
					<form:input path="respondentKey" cssErrorClass="error"
						class="form-control" />
				</div>
			</div>
			<form:button type="submit" class="btn btn-default">Přidat respondenta</form:button>
		</form:form>
	</div>
	<div class="panel-footer">
	<h3>Informace</h3>
	<p>Identifikátor uživatele musí být ve tvaru XXXX-XXXX-XXXX-XXXX, kde v roli X může být písmeno, nebo číslo. </p>
	<p>Při použití identifikátoru 0000-0000-0000-0000 je respondent označen jako anonymní.</p>
	</div>
</div>


<%@include file="/WEB-INF/layout/footer.jsp"%>