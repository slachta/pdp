package eu.slachta.pdp.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import eu.slachta.pdp.model.bm.BourdonMobileTest;
import eu.slachta.pdp.model.server.Bourdontest;
import eu.slachta.pdp.model.server.Bourdontestpage;
import eu.slachta.pdp.model.server.Respondent;
import eu.slachta.pdp.model.server.Testtype;
import eu.slachta.pdp.service.BourdonTestService;

@Controller
@RequestMapping(value = "/api/test/bourdon")
public class RESTBourdonMobileController {
	private static final Logger logger = LoggerFactory.getLogger(RESTBourdonMobileController.class);
	
	@Resource(name = "bourdonTestService")
	private BourdonTestService bourdonService;

	/**
	 * Adds a bourdon test into database. It requires JSON data in bmTestsData
	 * key in HTTP POST and apiKey to identificate the device.
	 * 
	 * @param bourdonMobileTestsJSONData
	 * @param deviceApiKey
	 * @param body
	 * @param locale
	 * @param model
	 * @return
	 */

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ResponseEntity<String> addBourdonTest(
			@RequestParam("bmTestsData") String bourdonMobileTestsJSONData,
			@RequestParam("apiKey") String deviceApiKey,
			@RequestBody String body) {

		logger.info("JSON "+bourdonMobileTestsJSONData);
		Gson gson = new Gson();

		try{
			BourdonMobileTest[] bts = gson.fromJson(bourdonMobileTestsJSONData,BourdonMobileTest[].class);
			
			if (bts != null && bts.length > 0) {
				for(int i=0; i<bts.length; i++){
					Respondent r = new Respondent();
					r.setAge(bts[i].getBourdonMobileTestInformation().getAge());
					r.setEducationLevel(bts[i].getBourdonMobileTestInformation().getEducationLevel());
					r.setSex(bts[i].getBourdonMobileTestInformation().getSex());
					r.setName("");
					r.setSurname("");
					r.setRespondentKey(bts[i].getBourdonMobileTestInformation().getUserId());
					
					Testtype t = new Testtype();
					t.setId(1); //Bourdon test has ID 1
					
					Bourdontest bt = new Bourdontest();
					bt.setImagesPerPage(bts[i].getBourdonMobileTestInformation().getImagesPerPage());
					bt.setTimePerPage(bts[i].getBourdonMobileTestInformation().getTimePerPage());
					bt.setPages(bts[i].getBourdonMobileTestInformation().getPages());
					bt.setDate(new java.sql.Date(new Date((bts[i].getBourdonMobileTestInformation().getTime()/1000)*1000).getTime()));
					bt.setNote(bts[i].getBourdonMobileTestInformation().getNote());
					bt.setResolution(bts[i].getBourdonMobileTestInformation().getResolution());
					bt.setDpi(bts[i].getBourdonMobileTestInformation().getDpi());
					bt.setOrientation(bts[i].getBourdonMobileTestInformation().getOrientation());
					bt.setCellSize(bts[i].getBourdonMobileTestInformation().getCellSize());
					bt.setRefImg1Id(bts[i].getBourdonMobileTestInformation().getRefImg1Id());
					bt.setRefImg2Id(bts[i].getBourdonMobileTestInformation().getRefImg2Id());
					bt.setRefImg3Id(bts[i].getBourdonMobileTestInformation().getRefImg3Id());
					bt.setApiKey(deviceApiKey);
					
					ArrayList<Bourdontestpage> albtpd = new ArrayList<Bourdontestpage>();
					
					for(int j=0; j<bts[i].getBourdonMobileTestData().size(); j++){
						Bourdontestpage btp = new Bourdontestpage();
						btp.setPageNumber(bts[i].getBourdonMobileTestData().get(j).getPageNumber());
						btp.setWrongSetCount(bts[i].getBourdonMobileTestData().get(j).getWrongSetCount());
						btp.setWrongAllCount(bts[i].getBourdonMobileTestData().get(j).getWrongAllCount());
						btp.setRightSetCount(bts[i].getBourdonMobileTestData().get(j).getRightSetCount());
						btp.setRightAllCount(bts[i].getBourdonMobileTestData().get(j).getRightAllCount());
						btp.setTimeSpent(bts[i].getBourdonMobileTestData().get(j).getTimeSpent());
						btp.setBourdontest(bt);
						albtpd.add(btp);
					}
					
					bt.setBourdontestpages(albtpd);
					bt.setRespondent(r);
					bt.setTesttype(t);
					
					List<Bourdontest> a = new ArrayList<Bourdontest>();
					r.setBourdontests(a);
					
					bourdonService.createTest(r, bt, albtpd);
				}
				
			}
			
			logger.info("Bourdon tests added.");
			return new ResponseEntity<String>(bourdonMobileTestsJSONData,HttpStatus.OK);
		}catch(JsonSyntaxException jse){
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * Returns a Bourdon test with given ID. URI for test retrieval is
	 * /api/test/bourdon/delete/{id}.
	 * 
	 * @param testId
	 * @return
	 */
	
	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	@ResponseBody
	public String getAllBourdonTests() {
		return bourdonService.getTestsInJSON();
	}

	/**
	 * Returns a BourdonTest with given ID in JSON data. API path is
	 * /api/test/bourdon/get/{id}.
	 * 
	 * @param testId
	 * @return
	 */
	
	@RequestMapping(value = "/get/{testId}", method = RequestMethod.GET)
	@ResponseBody
	public String getBourdonTest(@PathVariable("testId") int testId) {
		return bourdonService.getTestInJSON(testId);
	}

	/**
	 * Deletes Bourdon test from database. URI for the deletion is
	 * /api/test/bourdon/delete/{id}.
	 * 
	 * @param testId
	 * @return
	 */
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public String deleteBourdonTest(@PathVariable("id") int testId) {
		if(bourdonService.findById(testId) != null){
			bourdonService.delete(testId);
			return "Bourdon test deleted. ID "+testId;
		}else{
			return "Bourdon does not exist with ID "+testId;
		}
	}
}