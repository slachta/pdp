package eu.slachta.pdp.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import eu.slachta.pdp.form.StatisticsChoices;
import eu.slachta.pdp.model.server.Bourdonnorm;
import eu.slachta.pdp.model.server.Bourdontest;
import eu.slachta.pdp.model.server.Bourdontestpage;
import eu.slachta.pdp.service.BourdonNormService;
import eu.slachta.pdp.service.BourdonTestPageService;
import eu.slachta.pdp.service.BourdonTestService;
import eu.slachta.pdp.service.RespondentService;
import eu.slachta.pdp.stats.ExploratoryModel;
import eu.slachta.pdp.stats.ExploratoryStatMaker;

@Controller
public class BourdonStatisticsController {
	@Resource(name = "bourdonTestPageService")
	private BourdonTestPageService bourdonTestPageService;

	@Resource(name = "bourdonTestService")
	private BourdonTestService bourdonTestService;

	@Resource(name = "respondentService")
	private RespondentService respondentService;

	@Resource(name = "bourdonNormService")
	private BourdonNormService bourdonNormService;

	@RequestMapping(value = "/statistics/bourdon", method = RequestMethod.GET)
	public String redir(ModelMap model, Principal principal) {
		return "redirect:/statistics/bourdon/list";
	}

	@RequestMapping("/statistics/bourdon/list")
	public String getListOfTestsForEvaluation(ModelMap model) {
		List<Bourdontest> bt = bourdonTestService.findAll();

		model.addAttribute("tests", bt);
		model.addAttribute("bourdonStatsTestChoices", new StatisticsChoices());

		return "bourdonStatsList";
	}

	@RequestMapping("/statistics/bourdon/exploratory/{id}")
	public String getBourdonExploratoryForSingleTest(@PathVariable Integer id,
			ModelMap model) {

		ArrayList<Bourdontestpage> btps = bourdonTestPageService
				.findByTestId(id);

		Bourdonnorm fNorm = bourdonNormService
				.findByName("bourdonFruitfulnessNorm");
		Bourdonnorm eNorm = bourdonNormService
				.findByName("bourdonErrorRateNorm");
		Bourdonnorm tNorm = bourdonNormService
				.findByName("bourdonTimeSpentNorm");

		model.addAttribute("fNorm", fNorm);
		model.addAttribute("eNorm", eNorm);
		model.addAttribute("tNorm", tNorm);

		model.addAttribute("fruitfulnessData", createFruitfulnessData(id, btps));
		model.addAttribute("errorRateData", createErrorrateData(id, btps));
		model.addAttribute("timeSpentData", createTimespentData(id, btps));

		return "bourdonExploratorySingleTest";
	}

	private ExploratoryModel createTimespentData(int testId,
			ArrayList<Bourdontestpage> btps) {

		ArrayList<Double> timeSpent = new ArrayList<Double>();

		for (Bourdontestpage page : btps) {
			timeSpent.add((double) page.getTimeSpent());
		}

		ExploratoryStatMaker statMaker = new ExploratoryStatMaker(testId,
				timeSpent);

		return statMaker.getExploratoryData();
	}

	private ExploratoryModel createErrorrateData(int testId,
			ArrayList<Bourdontestpage> btps) {

		ArrayList<Integer> wrongSetCount = new ArrayList<Integer>();
		for (Bourdontestpage page : btps) {
			wrongSetCount.add(page.getWrongSetCount());
		}

		ArrayList<Integer> wrongAllCount = new ArrayList<Integer>();
		for (Bourdontestpage page : btps) {
			wrongAllCount.add(page.getWrongAllCount());
		}

		ArrayList<Double> errorrate = new ArrayList<Double>();
		for (int i = 0; i < wrongSetCount.size(); i++) {
			double percentage = ((double) wrongSetCount.get(i) / (double) wrongAllCount
					.get(i)) * 100;

			errorrate.add(percentage);
		}

		ExploratoryStatMaker statMaker = new ExploratoryStatMaker(testId,
				errorrate);

		return statMaker.getExploratoryData();
	}

	private ExploratoryModel createFruitfulnessData(int testId,
			ArrayList<Bourdontestpage> btps) {

		ArrayList<Integer> rightSetCount = new ArrayList<Integer>();
		for (Bourdontestpage page : btps) {
			rightSetCount.add(page.getRightSetCount());
		}

		ArrayList<Integer> rightAllCount = new ArrayList<Integer>();
		for (Bourdontestpage page : btps) {
			rightAllCount.add(page.getRightAllCount());
		}

		ArrayList<Double> fruitfulness = new ArrayList<Double>();
		for (int i = 0; i < rightSetCount.size(); i++) {
			double percentage = ((double) rightSetCount.get(i) / (double) rightAllCount
					.get(i)) * 100;

			fruitfulness.add(percentage);
		}

		ExploratoryStatMaker statMaker = new ExploratoryStatMaker(testId,
				fruitfulness);
		return statMaker.getExploratoryData();
	}

	@RequestMapping("/statistics/bourdon/multipleexploratory")
	public String getBourdonMultipleExploratory(@RequestParam String action,
			StatisticsChoices stats, ModelMap model) {
		int[] testIDs = stats.getIds();

		if (action.equals("compare")) {
			if (testIDs.length > 0) {
				passMultipleSeriesData(model, testIDs);

				Bourdonnorm fNorm = bourdonNormService
						.findByName("bourdonFruitfulnessNorm");
				Bourdonnorm eNorm = bourdonNormService
						.findByName("bourdonErrorRateNorm");
				Bourdonnorm tNorm = bourdonNormService
						.findByName("bourdonTimeSpentNorm");

				model.addAttribute("fNorm", fNorm);
				model.addAttribute("eNorm", eNorm);
				model.addAttribute("tNorm", tNorm);
			}
			return "bourdonExploratoryMultipleTest";
		} else if (action.equals("norm")) {
			if (testIDs.length > 0) {
				passNormSelectionData(model, testIDs);
			}

			return "bourdonNorm";
		} else if (action.equals("allnorm")) {
			passNormAllData(model);

			return "bourdonNorm";
		} else {
			if (testIDs.length > 0) {
				passMultipleSeriesData(model, testIDs);

				Bourdonnorm fNorm = bourdonNormService
						.findByName("bourdonFruitfulnessNorm");
				Bourdonnorm eNorm = bourdonNormService
						.findByName("bourdonErrorRateNorm");
				Bourdonnorm tNorm = bourdonNormService
						.findByName("bourdonTimeSpentNorm");

				model.addAttribute("fNorm", fNorm);
				model.addAttribute("eNorm", eNorm);
				model.addAttribute("tNorm", tNorm);
			}

			return "bourdonExploratoryMultipleTest";
		}
	}

	private void passNormSelectionData(ModelMap model, int[] testIDs) {
		if (testIDs.length > 0) {
			int id;
			ArrayList<Bourdontestpage> btps = new ArrayList<Bourdontestpage>();

			for (int i = 0; i < testIDs.length; i++) {
				id = testIDs[i];
				btps.addAll(bourdonTestPageService.findByTestId(id));
			}

			ArrayList<Double> fruitfulness = new ArrayList<Double>();
			ArrayList<Double> errorRate = new ArrayList<Double>();
			ArrayList<Double> timeSpent = new ArrayList<Double>();

			for (int i = 0; i < btps.size(); i++) {
				fruitfulness
						.add(((double) btps.get(i).getRightSetCount() / (double) btps
								.get(i).getRightAllCount()) * 100);
				errorRate
						.add(((double) btps.get(i).getWrongSetCount() / (double) btps
								.get(i).getWrongAllCount()) * 100);
				timeSpent.add((double) btps.get(i).getTimeSpent());
			}

			ExploratoryStatMaker statMaker = new ExploratoryStatMaker(0,
					fruitfulness);
			ExploratoryModel fruitfulnessExpl = statMaker.getExploratoryData();

			statMaker = new ExploratoryStatMaker(0, errorRate);
			ExploratoryModel errorRateExpl = statMaker.getExploratoryData();

			statMaker = new ExploratoryStatMaker(0, timeSpent);
			ExploratoryModel timeSpentExpl = statMaker.getExploratoryData();

			model.addAttribute("fruitfulnessData", fruitfulnessExpl);
			model.addAttribute("errorRateData", errorRateExpl);
			model.addAttribute("timeSpentData", timeSpentExpl);

			Bourdonnorm refFNorm = bourdonNormService
					.findByName("bourdonFruitfulnessNorm");
			if (refFNorm == null) {
				refFNorm = new Bourdonnorm();
			}

			refFNorm.setName("bourdonFruitfulnessNorm");
			refFNorm.setNumberOfSamples(fruitfulnessExpl.getSize());
			refFNorm.setMinimum(fruitfulnessExpl.getMinimum());
			refFNorm.setMaximum(fruitfulnessExpl.getMaximum());
			refFNorm.setMean(fruitfulnessExpl.getMean());
			refFNorm.setMedian(fruitfulnessExpl.getMedian());
			refFNorm.setUpperQuartile(fruitfulnessExpl.getUpperQuartile());
			refFNorm.setLowerQuartile(fruitfulnessExpl.getLowerQuartile());
			refFNorm.setIqr(fruitfulnessExpl.getIQR());
			bourdonNormService.updateNorm(refFNorm);

			Bourdonnorm refENorm = bourdonNormService
					.findByName("bourdonErrorRateNorm");
			if (refENorm == null) {
				refENorm = new Bourdonnorm();
			}
			refENorm.setName("bourdonErrorRateNorm");
			refENorm.setNumberOfSamples(errorRateExpl.getSize());
			refENorm.setMinimum(errorRateExpl.getMinimum());
			refENorm.setMaximum(errorRateExpl.getMaximum());
			refENorm.setMean(errorRateExpl.getMean());
			refENorm.setMedian(errorRateExpl.getMedian());
			refENorm.setUpperQuartile(errorRateExpl.getUpperQuartile());
			refENorm.setLowerQuartile(errorRateExpl.getLowerQuartile());
			refENorm.setIqr(errorRateExpl.getIQR());
			bourdonNormService.updateNorm(refENorm);

			Bourdonnorm refTNorm = bourdonNormService
					.findByName("bourdonTimeSpentNorm");
			if (refTNorm == null) {
				refTNorm = new Bourdonnorm();
			}

			refTNorm.setName("bourdonTimeSpentNorm");
			refTNorm.setNumberOfSamples(timeSpentExpl.getSize());
			refTNorm.setMinimum(timeSpentExpl.getMinimum());
			refTNorm.setMaximum(timeSpentExpl.getMaximum());
			refTNorm.setMean(timeSpentExpl.getMean());
			refTNorm.setMedian(timeSpentExpl.getMedian());
			refTNorm.setUpperQuartile(timeSpentExpl.getUpperQuartile());
			refTNorm.setLowerQuartile(timeSpentExpl.getLowerQuartile());
			refTNorm.setIqr(timeSpentExpl.getIQR());
			bourdonNormService.updateNorm(refTNorm);
		}
	}

	private void passNormAllData(ModelMap model) {
		ArrayList<Bourdontestpage> btps = new ArrayList<Bourdontestpage>();

		btps.addAll(bourdonTestPageService.findAll());

		ArrayList<Double> fruitfulness = new ArrayList<Double>();
		ArrayList<Double> errorRate = new ArrayList<Double>();
		ArrayList<Double> timeSpent = new ArrayList<Double>();

		for (int i = 0; i < btps.size(); i++) {
			fruitfulness
					.add(((double) btps.get(i).getRightSetCount() / (double) btps
							.get(i).getRightAllCount()) * 100);
			errorRate
					.add(((double) btps.get(i).getWrongSetCount() / (double) btps
							.get(i).getWrongAllCount()) * 100);
			timeSpent.add((double) btps.get(i).getTimeSpent());
		}

		ExploratoryStatMaker statMaker = new ExploratoryStatMaker(0,
				fruitfulness);
		ExploratoryModel fruitfulnessExpl = statMaker.getExploratoryData();

		statMaker = new ExploratoryStatMaker(0, errorRate);
		ExploratoryModel errorRateExpl = statMaker.getExploratoryData();

		statMaker = new ExploratoryStatMaker(0, timeSpent);
		ExploratoryModel timeSpentExpl = statMaker.getExploratoryData();

		model.addAttribute("fruitfulnessData", fruitfulnessExpl);
		model.addAttribute("errorRateData", errorRateExpl);
		model.addAttribute("timeSpentData", timeSpentExpl);

		Bourdonnorm refFNorm = bourdonNormService
				.findByName("bourdonFruitfulnessNorm");
		if (refFNorm == null) {
			refFNorm = new Bourdonnorm();
		}

		refFNorm.setName("bourdonFruitfulnessNorm");
		refFNorm.setNumberOfSamples(fruitfulnessExpl.getSize());
		refFNorm.setMinimum(fruitfulnessExpl.getMinimum());
		refFNorm.setMaximum(fruitfulnessExpl.getMaximum());
		refFNorm.setMean(fruitfulnessExpl.getMean());
		refFNorm.setMedian(fruitfulnessExpl.getMedian());
		refFNorm.setUpperQuartile(fruitfulnessExpl.getUpperQuartile());
		refFNorm.setLowerQuartile(fruitfulnessExpl.getLowerQuartile());
		refFNorm.setIqr(fruitfulnessExpl.getIQR());
		bourdonNormService.updateNorm(refFNorm);

		Bourdonnorm refENorm = bourdonNormService
				.findByName("bourdonErrorRateNorm");
		if (refENorm == null) {
			refENorm = new Bourdonnorm();
		}
		refENorm.setName("bourdonErrorRateNorm");
		refENorm.setNumberOfSamples(errorRateExpl.getSize());
		refENorm.setMinimum(errorRateExpl.getMinimum());
		refENorm.setMaximum(errorRateExpl.getMaximum());
		refENorm.setMean(errorRateExpl.getMean());
		refENorm.setMedian(errorRateExpl.getMedian());
		refENorm.setUpperQuartile(errorRateExpl.getUpperQuartile());
		refENorm.setLowerQuartile(errorRateExpl.getLowerQuartile());
		refENorm.setIqr(errorRateExpl.getIQR());
		bourdonNormService.updateNorm(refENorm);

		Bourdonnorm refTNorm = bourdonNormService
				.findByName("bourdonTimeSpentNorm");
		if (refTNorm == null) {
			refTNorm = new Bourdonnorm();
		}

		refTNorm.setName("bourdonTimeSpentNorm");
		refTNorm.setNumberOfSamples(timeSpentExpl.getSize());
		refTNorm.setMinimum(timeSpentExpl.getMinimum());
		refTNorm.setMaximum(timeSpentExpl.getMaximum());
		refTNorm.setMean(timeSpentExpl.getMean());
		refTNorm.setMedian(timeSpentExpl.getMedian());
		refTNorm.setUpperQuartile(timeSpentExpl.getUpperQuartile());
		refTNorm.setLowerQuartile(timeSpentExpl.getLowerQuartile());
		refTNorm.setIqr(timeSpentExpl.getIQR());
		bourdonNormService.updateNorm(refTNorm);
	}

	private void passMultipleSeriesData(ModelMap model, int[] testIDs) {
		int id;
		ArrayList<Bourdontestpage> btps;

		ArrayList<ExploratoryModel> multipleFruitfulnessData = new ArrayList<ExploratoryModel>();
		ArrayList<ExploratoryModel> multipleErrorrateData = new ArrayList<ExploratoryModel>();
		ArrayList<ExploratoryModel> multipleTimespentData = new ArrayList<ExploratoryModel>();

		for (int i = 0; i < testIDs.length; i++) {
			id = testIDs[i];

			btps = bourdonTestPageService.findByTestId(id);

			multipleFruitfulnessData.add(createFruitfulnessData(id, btps));
			multipleErrorrateData.add(createErrorrateData(id, btps));
			multipleTimespentData.add(createTimespentData(id, btps));
		}

		model.addAttribute("multipleFruitfulnessData", multipleFruitfulnessData);
		model.addAttribute("multipleErrorRateData", multipleErrorrateData);
		model.addAttribute("multipleTimeSpentData", multipleTimespentData);
	}
}
