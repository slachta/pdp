INSERT INTO `users` (`id`, `username`, `password`, `enabled`) VALUES
(1, 'admin@pdp.slachta.eu', 'pdp', 1),
(2, 'anonymous', 'Vk7r5PwKhdqQgAw', 1);

INSERT INTO `authorities` (`id`, `authority`) VALUES
(1, 'ROLE_ADMIN'),
(2, 'ROLE_USER');

INSERT INTO `testtype` (`id`, `name`, `prefix`, `description`) VALUES
(1, 'Bourdonův test', 'bourdon', NULL);
