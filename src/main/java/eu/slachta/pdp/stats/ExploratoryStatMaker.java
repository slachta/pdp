package eu.slachta.pdp.stats;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;

public class ExploratoryStatMaker {
	private ArrayList<Double> data;
	private ExploratoryModel model;
	private int id;

	public ExploratoryStatMaker(int testId, ArrayList<Double> data) {
		this.setData(data);
		this.model = new ExploratoryModel();
		this.id = testId;

		createExploratoryDataSet();
	}

	private void createExploratoryDataSet() {
		Collections.sort(this.data);

		model.setSize(this.data.size());
		if(this.data.size()>=8){
			model.setMedian(round(median(), 2));
			model.setMean(round(mean(), 2));
			Double lowerQuartile = lowerQuartile();
			model.setLowerQuartile(round(lowerQuartile, 2));
			Double upperQuartile = upperQuartile();
			model.setUpperQuartile(round(upperQuartile, 2));
			model.setMaximum(round(Collections.max(this.data), 2));
			model.setMinimum(round(Collections.min(this.data), 2));
			model.setIQR(round(upperQuartile - lowerQuartile, 2));	
		}else{
			model.setMedian(-1d);
			model.setMean(-1d);
			model.setLowerQuartile(-1d);
			model.setUpperQuartile(-1d);
			model.setMaximum(-1d);
			model.setMinimum(-1d);
			model.setIQR(-1d);
		}
		model.setId(this.id);
	}

	/**
	 * @return the data
	 */
	public ArrayList<Double> getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(ArrayList<Double> data) {
		this.data = data;
	}

	public ExploratoryModel getExploratoryData() {
		return model;
	}

	private Double median() {
		int size = this.data.size();
		int middle = size / 2;

		if (this.data.size() % 2 == 1) {
			return ((double) this.data.get(middle) + (double) this.data
					.get(middle + 1)) / 2;
		} else {
			return (double) this.data.get(middle);
		}
	}

	private Double lowerQuartile() {
		int size = this.data.size();
		int lower = size / 4;

		if (size % 2 == 1) {
			return (double) this.data.get(lower);
		} else {
			return ((double) this.data.get(lower + 1) + (double) this.data
					.get(lower)) / 2;
		}
	}

	private Double upperQuartile() {
		int size = this.data.size();
		int upper = (size / 2) + (size / 4);

		if (size % 2 == 1) {
			return (double) this.data.get(upper);
		} else {
			return ((double) this.data.get(upper + 1) + (double) this.data
					.get(upper)) / 2;
		}
	}

	private Double mean() {
		double sum = 0;

		for (int i = 0; i < this.data.size(); i++) {
			sum += this.data.get(i);
		}

		return (double) (sum / this.data.size());
	}

	private double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}
}
