package eu.slachta.pdp.service;

import java.util.List;
import java.util.ArrayList;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import eu.slachta.pdp.model.server.Bourdontestpage;
import eu.slachta.pdp.repository.BourdonTestPagesRepository;

@Service
@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
public class BourdonTestPageService {

	@Resource
	BourdonTestPagesRepository bourdonTestPagesDao;

	public void createBourdonTestPage(Bourdontestpage page) {
		bourdonTestPagesDao.save(page);
	}

	public void deleteBourdonTestPage(int id) {
		bourdonTestPagesDao.delete(id);
	}

	public Bourdontestpage findById(int id) {
		return bourdonTestPagesDao.findOne(id);
	}
	
	public ArrayList<Bourdontestpage> findByTestId(int id){
		return bourdonTestPagesDao.findByTestId(id);
	}

	public List<Bourdontestpage> findAll() {
		return bourdonTestPagesDao.findAll();
	}
}
