package eu.slachta.pdp.repository;

import java.util.ArrayList;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import eu.slachta.pdp.model.server.Respondent;

public interface RespondentRepository extends JpaRepository<Respondent, Integer> {
	
	@Query("SELECT r FROM Respondent r WHERE r.respondentKey = :respondentKey ")
	public ArrayList<Respondent> findByRespondentKey(@Param("respondentKey") String respondentKey);
	
	@Query("SELECT r FROM Respondent r ORDER BY r.id DESC")
	public ArrayList<Respondent> findLastAddedRespondents(Pageable page);
}
