<%@ page pageEncoding="UTF-8" language="java"
	contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/layout/header.jsp"%>

<div class="row row-offcanvas row-offcanvas-right">
	<div class="col-xs-12 col-sm-9">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="bs-example bs-example-tabs">
					<ul class="nav nav-tabs" id="myTab">
						<li class="active"><a data-toggle="tab" href="#params">Parametry
								testu</a></li>

						<li><a data-toggle="tab" href="#graphs">Grafy</a></li>

						<li><a data-toggle="tab" href="#results">Výsledky</a></li>
					</ul>

					<div class="tab-content" id="myTabContent">
						<div class="tab-pane fade in active" id="params">
						<c:if test="${not empty test}">
							<h2 id="tabs-examples">Informace testu</h2>
							<dl class="dl-horizontal">
								<dt>ID testu:</dt>
								<dd>${fn:escapeXml(test.id)}</dd>
								<dt>API klíč zařízení:</dt>
								<dd>${fn:escapeXml(test.apiKey)}</dd>
								<dt>Datum testu:</dt>
								<dd>${fn:escapeXml(test.date)}</dd>
								<dt>Respondent:</dt>
								<dd><a
									href="${pageContext.request.contextPath}/respondent/${fn:escapeXml(test.respondent.id)}">${fn:escapeXml(test.respondent.id)}</a></dd>
								<dt>Rozlišení:</dt>
								<dd>${fn:escapeXml(test.resolution)}</dd>
								<dt>DPI:</dt>
								<dd>${fn:escapeXml(test.dpi)}</dd>
								<dt>Orientace displeje:</dt>
								<dd>${fn:escapeXml(test.orientation)}</dd>
								<dt>Proveden celý test:</dt>
								<dd><c:choose><c:when test="${fn:length(testPages) == test.pages}">ano</c:when><c:otherwise>ne</c:otherwise></c:choose></dd>
							</dl>
							<h2 id="tabs-examples">Parametry testu</h2>
							<dl class="dl-horizontal">
								<dt>Počet stránek testu:</dt>
								<dd>${fn:escapeXml(test.pages)}</dd>
								<dt>Čas na stranu testu:</dt>
								<dd>${fn:escapeXml(test.timePerPage)}</dd>
								<dt>Obrázků na stranu:</dt>
								<dd>${fn:escapeXml(test.imagesPerPage)}</dd>
								<dt>Velikost buňky (v Px):</dt>
								<dd>${fn:escapeXml(test.cellSize)}</dd>
								<dt>Referenční obrázky:</dt>
								<dd>
									<div class="row">
										<div class="col-xs-6 col-md-3">
											<div class="thumbnail">
												<img
													src="${pageContext.request.contextPath}/resources/img-bm/${test.refImg1Id}.png"
													alt="...">
												<div class="caption">
													ID ${test.refImg1Id}
												</div>
											</div>
										</div>
										<div class="col-xs-6 col-md-3">
											<div class="thumbnail">
												<img
													src="${pageContext.request.contextPath}/resources/img-bm/${test.refImg2Id}.png"
													alt="...">
												<div class="caption">
													ID ${test.refImg2Id}
												</div>
											</div>
										</div>
										<div class="col-xs-6 col-md-3">
											<div class="thumbnail">
												<img
													src="${pageContext.request.contextPath}/resources/img-bm/${test.refImg3Id}.png"
													alt="...">
												<div class="caption">
													ID ${test.refImg3Id}
												</div>
											</div>
										</div>
									</div>
								</dd>
							</dl>
							</c:if>
							<c:if test="${empty test}">
								<div class="panel-body">
									<p>Test s požadovaným ID neexistuje.</p>
								</div>
							</c:if>
						</div>

						<div class="tab-pane fade" id="graphs">
							<h2 id="tabs-examples">Úspěšnost při volbě obrázků</h2>
							<c:if test="${not empty testPages}">
								<div class="chart" id="div_g1"
									style="width: 600px; height: 300px;"></div>
							</c:if>
							<c:if test="${empty testPages}">
								<div class="panel-body">
									<p>Vzhledem k tomu, že tento test nemá žádné stránky,
										nemůžeme zobrazit v grafech žádná data.</p>
								</div>
							</c:if>
							<h2 id="tabs-examples">Chybovost při volbě obrázků</h2>
							<c:if test="${not empty testPages}">
								<div class="chart" id="div_g2"
									style="width: 600px; height: 300px;"></div>
							</c:if>
							<c:if test="${empty testPages}">
								<div class="panel-body">
									<p>Vzhledem k tomu, že tento test nemá žádné stránky,
										nemůžeme zobrazit v grafech žádná data.</p>
								</div>
							</c:if>
							<h2 id="tabs-examples">Čas věnovaný straně testu</h2>
							<c:if test="${not empty testPages}">
								<div class="chart" id="div_g3"
									style="width: 600px; height: 300px;"></div>
							</c:if>
							<c:if test="${empty testPages}">
								<div class="panel-body">
									<p>Vzhledem k tomu, že tento test nemá žádné stránky,
										nemůžeme zobrazit v grafech žádná data.</p>
								</div>
							</c:if>
							<h2 id="tabs-examples">Kumulativní procentuální graf</h2>
							<c:if test="${not empty testPages}">
								<div class="chart" id="div_g4"
									style="width: 600px; height: 300px;"></div>
							</c:if>
							<c:if test="${empty testPages}">
								<div class="panel-body">
									<p>Vzhledem k tomu, že tento test nemá žádné stránky,
										nemůžeme zobrazit v grafech žádná data.</p>
								</div>
							</c:if>
						</div>

						<div class="tab-pane fade" id="results">
							<c:if test="${not empty testPages}">
								<table class="table table-hover">
									<thead>
										<tr>
											<th>Číslo stránky [pořadí]</th>
											<th>Všechny správné obrázky [počet]</th>
											<th>Správně zvolené obrázky [počet]</th>
											<th>Všechny špatné obrázky [počet]</th>
											<th>Špatně zvolené obrázky [počet]</th>
											<th>Čas věnovaný straně testu [ms]</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${testPages}" var="page">
											<tr>
												<td>${fn:escapeXml(page.pageNumber)}</td>
												<td>${fn:escapeXml(page.rightAllCount)}</td>
												<td>${fn:escapeXml(page.rightSetCount)}</td>
												<td>${fn:escapeXml(page.wrongAllCount)}</td>
												<td>${fn:escapeXml(page.wrongSetCount)}</td>
												<td>${fn:escapeXml(page.timeSpent)}</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</c:if>
							<c:if test="${empty testPages}">
								<div class="panel-body">
									<p>Tento test nemá žádné stránky.</p>
								</div>
							</c:if>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /example -->
	</div>

	<div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar"
		role="navigation">
		<div class="panel panel-default">
			<!-- Default panel contents -->
			<div class="panel-heading">Bourdonův test</div>
			<nav class="nav-sidebar">
				<ul class="nav">
					<li><a
						href="${pageContext.request.contextPath}/results/bourdon/all">Všechny
							testy</a></li>
					<li><a href="#">Hledání testů</a></li>
				</ul>
			</nav>
		</div>
	</div>
	<!--/span-->
</div>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery-1.11.0.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/dygraph-combined.js"></script>

<script type="text/javascript">
	g1 = new Dygraph(document.getElementById("div_g1"), ${fruitfulnessGraph}, {
		legend : 'always',
		ylabel : 'Počet obrázků',
		xlabel : 'Strana testu',
		labelsDivStyles : {
			'text-align' : 'right',
			'background' : 'none'
		},
		interactionModel: {}
	});

	g2 = new Dygraph(document.getElementById("div_g2"), ${errorRateGraph}, {
		legend : 'always',
		ylabel : 'Počet obrázků',
		xlabel : 'Strana testu',
		labelsDivStyles : {
			'text-align' : 'right',
			'background' : 'none'
		},
		interactionModel: {}
	});

	g3 = new Dygraph(document.getElementById("div_g3"), ${timeGraph}, {
		legend : 'always',
		ylabel : 'Čas [ms]',
		xlabel : 'Strana testu',
		labelsDivStyles : {
			'text-align' : 'right',
			'background' : 'none'
		},
		interactionModel: {}
	});

	g4 = new Dygraph(document.getElementById("div_g4"), ${cumulativeGraph}, {
		legend : 'always',
		ylabel : 'Procentuální podíl [%]',
		xlabel : 'Strana testu',
		labelsDivStyles : {
			'text-align' : 'right',
			'background' : 'none'
		},
		interactionModel: {}
	});

	$(function () {
		$('#myTab a:first').tab('show')
		})
		 
	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		  var target = $(e.target).attr("href");
		  if ((target == '#graphs')) {
			  g1.resize();
			  g2.resize();
			  g3.resize();
			  g4.resize();
		  }
	});
</script>

<%@include file="/WEB-INF/layout/footer-wo-js.jsp"%>