package eu.slachta.pdp.model.bm;

public class BourdonMobileTestPageData {
	private int pageNumber;
	private int wrongSetCount;
	private int rightSetCount;
	private int wrongAllCount;
	private int rightAllCount;
	private int timeSpent;
	private long pageId;
	private long testId;
	
	public BourdonMobileTestPageData() {
	}
	
	public BourdonMobileTestPageData(long page_id,long test_id,int pageNumber,int wrongSetCount,int rightSetCount,int wrongAllCount,int rightAllCount,int timeSpent) {
		this.pageNumber=pageNumber;
		this.wrongSetCount=wrongSetCount;
		this.rightSetCount=rightSetCount;
		this.wrongAllCount=wrongAllCount;
		this.rightAllCount=rightAllCount;
		this.timeSpent=timeSpent;
		this.pageId=page_id;
		this.testId=test_id;
	}
	
	public long getTestId() {
		return testId;
	}

	public void setTestId(long test_id) {
		this.testId = test_id;
	}

	public long getPageId() {
		return pageId;
	}

	public void setPageId(long page_id) {
		this.pageId = page_id;
	}
	
	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getTimeSpent() {
		return timeSpent;
	}

	public void setTimeSpent(int timeSpent) {
		this.timeSpent = timeSpent;
	}
	
	public int getWrongSetCount() {
		return wrongSetCount;
	}

	public void setWrongSetCount(int wrongSetCount) {
		this.wrongSetCount = wrongSetCount;
	}

	public int getRightSetCount() {
		return rightSetCount;
	}

	public void setRightSetCount(int rightSetCount) {
		this.rightSetCount = rightSetCount;
	}

	public int getWrongAllCount() {
		return wrongAllCount;
	}

	public void setWrongAllCount(int wrongAllCount) {
		this.wrongAllCount = wrongAllCount;
	}
	
	public int getRightAllCount() {
		return rightAllCount;
	}

	public void setRightAllCount(int rightAllCount) {
		this.rightAllCount = rightAllCount;
	}
}
