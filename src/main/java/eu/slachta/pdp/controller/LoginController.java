package eu.slachta.pdp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {
	
	@RequestMapping("/")
	public String index(Model model) {
		return "redirect:/login";
	}
 
	@RequestMapping(value = { "login" }, method = { RequestMethod.GET })
	public String login(
			ModelMap model, 
			@RequestParam(value="authfailed", required=false) Boolean authfailed
			) 
	{
		if(authfailed!=null){
			model.addAttribute("authfailed", authfailed);			
		}
 
		return "login";
	}
 
	@RequestMapping(value="/logout", method = RequestMethod.GET)
	public String logout(ModelMap model) {
 
		return "login";
	}
}
