<%@ page pageEncoding="UTF-8" language="java"
	contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/layout/header.jsp"%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-6 col-md-4">
				<div class="thumbnail">
					<img
						src="${pageContext.request.contextPath}/resources/img/bourdon.png"
						alt="Bourdonův test">
					<div class="caption">
						<h3>Bourdonův test</h3>
						<p>Ve všech modifikacích Bourdonova testu se v zásadě jedná o
							řady navzájem si velice podobných symbolů mezi nimiž má proband
							za úkol diferencovat v průběhu delšího přesně stanoveného času.
							Jedná se o neverbální výkonový test, který neklade žádné nároky
							na jazykové znalosti nebo na zvláštní vědomosti.</p>
						<p>
							<a href="${pageContext.request.contextPath}/results/bourdon"
								class="btn btn-primary" role="button">Prohlížet data testů</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<%@include file="/WEB-INF/layout/footer.jsp"%>
