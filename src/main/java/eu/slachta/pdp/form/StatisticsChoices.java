package eu.slachta.pdp.form;

public class StatisticsChoices {
	private int[] ids;

	/**
	 * @return the ids
	 */
	public int[] getIds() {
		return ids;
	}

	/**
	 * @param ids the ids to set
	 */
	public void setIds(int[] ids) {
		this.ids = ids;
	}
}
