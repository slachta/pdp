<%@ page pageEncoding="UTF-8" language="java"
	contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/layout/header.jsp"%>

<div class="panel panel-default">
	<!-- Default panel contents -->
	<div class="panel-heading">Exploratorní analýza testu</div>

	<div class="panel-body">
		<c:if test="${not empty multipleFruitfulnessData}">
			<h2>Správnost výběru obrázků</h2>
			<div class="panel panel-default">
				<div class="panel-heading">Tabulka explorační analýzy</div>
				<div class="panel-body">
					<c:if test="${not empty multipleFruitfulnessData}">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>ID testu</th>
									<th>Počet vzorků</th>
									<th>Střední hodnota [%]</th>
									<th>Medián [%]</th>
									<th>Minimum [%]</th>
									<th>Maximum [%]</th>
									<th>IQR [%]</th>
									<th>Dolní kvartil [%]</th>
									<th>Horní kvartil [%]</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${multipleFruitfulnessData}"
									var="fruitfulnessData">
									<tr>
										<td>${fruitfulnessData.id}</td>
										<td>${fruitfulnessData.size}</td>
										<td>${fruitfulnessData.mean}</td>
										<td>${fruitfulnessData.median}</td>
										<td>${fruitfulnessData.minimum}</td>
										<td>${fruitfulnessData.maximum}</td>
										<td>${fruitfulnessData.IQR}</td>
										<td>${fruitfulnessData.lowerQuartile}</td>
										<td>${fruitfulnessData.upperQuartile}</td>
									</tr>
								</c:forEach>
								<c:if test="${not empty fNorm}">
									<tr>
										<td>Norma</td>
										<td>${fNorm.numberOfSamples}</td>
										<td>${fNorm.mean}</td>
										<td>${fNorm.median}</td>
										<td>${fNorm.minimum}</td>
										<td>${fNorm.maximum}</td>
										<td>${fNorm.iqr}</td>
										<td>${fNorm.lowerQuartile}</td>
										<td>${fNorm.upperQuartile}</td>
									</tr>
								</c:if>
							</tbody>
						</table>
					</c:if>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">Graf</div>
				<div class="panel-body">
					<div id="fruitfulness"></div>
				</div>
			</div>
			<h2>Neprávnost výběru obrázků</h2>
			<div class="panel panel-default">
				<div class="panel-heading">Tabulka explorační analýzy</div>
				<div class="panel-body">
					<c:if test="${not empty multipleErrorRateData}">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>ID testu</th>
									<th>Počet vzorků</th>
									<th>Střední hodnota [%]</th>
									<th>Medián [%]</th>
									<th>Minimum [%]</th>
									<th>Maximum [%]</th>
									<th>IQR [%]</th>
									<th>Dolní kvartil [%]</th>
									<th>Horní kvartil [%]</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${multipleErrorRateData}" var="errorRateData">
									<tr>
										<td>${errorRateData.id}</td>
										<td>${errorRateData.size}</td>
										<td>${errorRateData.mean}</td>
										<td>${errorRateData.median}</td>
										<td>${errorRateData.minimum}</td>
										<td>${errorRateData.maximum}</td>
										<td>${errorRateData.IQR}</td>
										<td>${errorRateData.lowerQuartile}</td>
										<td>${errorRateData.upperQuartile}</td>
									</tr>
								</c:forEach>
								<c:if test="${not empty eNorm}">
									<tr>
										<td>Norma</td>
										<td>${eNorm.numberOfSamples}</td>
										<td>${eNorm.mean}</td>
										<td>${eNorm.median}</td>
										<td>${eNorm.minimum}</td>
										<td>${eNorm.maximum}</td>
										<td>${eNorm.iqr}</td>
										<td>${eNorm.lowerQuartile}</td>
										<td>${eNorm.upperQuartile}</td>
									</tr>
								</c:if>
							</tbody>
						</table>
					</c:if>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">Graf</div>
				<div class="panel-body">
					<div id="errorrate"></div>
				</div>
			</div>

			<h2>Čas na stranu testu</h2>
			<div class="panel panel-default">
				<div class="panel-heading">Tabulka explorační analýzy</div>
				<div class="panel-body">
					<c:if test="${not empty multipleTimeSpentData}">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>ID testu</th>
									<th>Počet vzorků</th>
									<th>Střední hodnota [ms]</th>
									<th>Medián [ms]</th>
									<th>Minimum [ms]</th>
									<th>Maximum [ms]</th>
									<th>IQR [ms]</th>
									<th>Dolní kvartil [ms]</th>
									<th>Horní kvartil [ms]</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${multipleTimeSpentData}" var="timeSpentData">
									<tr>
										<td>${timeSpentData.id}</td>
										<td>${timeSpentData.size}</td>
										<td>${timeSpentData.mean}</td>
										<td>${timeSpentData.median}</td>
										<td>${timeSpentData.minimum}</td>
										<td>${timeSpentData.maximum}</td>
										<td>${timeSpentData.IQR}</td>
										<td>${timeSpentData.lowerQuartile}</td>
										<td>${timeSpentData.upperQuartile}</td>
									</tr>
								</c:forEach>
								<c:if test="${not empty tNorm}">
										<tr>
											<td>Norma</td>
											<td>${tNorm.numberOfSamples}</td>
											<td>${tNorm.mean}</td>
											<td>${tNorm.median}</td>
											<td>${tNorm.minimum}</td>
											<td>${tNorm.maximum}</td>
											<td>${tNorm.iqr}</td>
											<td>${tNorm.lowerQuartile}</td>
											<td>${tNorm.upperQuartile}</td>
										</tr>
									</c:if>
							</tbody>
						</table>
					</c:if>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">Graf</div>
				<div class="panel-body">
					<div id="timespent"></div>
				</div>
			</div>
		</c:if>
		<c:if test="${empty multipleFruitfulnessData}">
			<div class="alert alert-danger">Nevybrali jste žádný test.
				Nemáme data k zobrazení.</div>
		</c:if>
	</div>

</div>

<script
	src="${pageContext.request.contextPath}/resources/js/jquery-1.11.0.min.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/js/highcharts.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/js/highcharts-more.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/js/modules/exporting.js"></script>

<script type="text/javascript">
$(function() {
	$('#fruitfulness').highcharts({

		chart : {
			type : 'boxplot'
		},

		title : {
			text : 'Správné výběry obrázků na stranu testu'
		},

		legend : {
			enabled : false
		},

		xAxis : {
			categories : [ <c:forEach items="${multipleFruitfulnessData}" var="fruitfulnessData">'${fruitfulnessData.id}',</c:forEach><c:if test="${not empty fNorm}">'norma'</c:if> ],
			title : {
				text : 'ID Bourdonova testu.'
			}
		},

		yAxis : {
			title : {
				text : 'Procentuální podíl výběru'
			},
		},
		
			series : [ {
				name : 'Observations',
				data : [ <c:forEach items="${multipleFruitfulnessData}" var="fruitfulnessData">[ ${fruitfulnessData.minimum}, ${fruitfulnessData.lowerQuartile}, ${fruitfulnessData.median}, ${fruitfulnessData.upperQuartile}, ${fruitfulnessData.maximum} ],</c:forEach>
				<c:if test="${not empty fNorm}">[ ${fNorm.minimum}, 			${fNorm.lowerQuartile}, 			${fNorm.median},			${fNorm.upperQuartile},				${fNorm.maximum} 	],</c:if> ],
				tooltip : {
					headerFormat : '<em>Experiment No {point.key}</em><br/>'
				}
			} ]
		});
	});
</script>

<script type="text/javascript">
$(function() {
	$('#errorrate').highcharts({

		chart : {
			type : 'boxplot'
		},

		title : {
			text : 'Nesprávné výběry obrázků na stranu testu'
		},

		legend : {
			enabled : false
		},

		xAxis : {
			categories : [ <c:forEach items="${multipleErrorRateData}" var="errorRateData">'${errorRateData.id}',</c:forEach><c:if test="${not empty fNorm}">'norma'</c:if> ],
			title : {
				text : 'ID Bourdonova testu.'
			}
		},

		yAxis : {
			title : {
				text : 'Procentuální podíl výběru'
			},
		},
		
			series : [ {
				name : 'Observations',
				data : [ <c:forEach items="${multipleErrorRateData}" var="errorRateData">[ ${errorRateData.minimum}, ${errorRateData.lowerQuartile}, ${errorRateData.median}, ${errorRateData.upperQuartile}, ${errorRateData.maximum} ],</c:forEach>
				<c:if test="${not empty eNorm}">[ ${eNorm.minimum}, 			${eNorm.lowerQuartile}, 			${eNorm.median},			${eNorm.upperQuartile},				${eNorm.maximum} 	],</c:if> ],
				tooltip : {
					headerFormat : '<em>Experiment No {point.key}</em><br/>'
				}
			} ]
		});
	});
</script>

<script type="text/javascript">
	$(function() {
		$('#timespent').highcharts({

			chart : {
				type : 'boxplot'
			},

			title : {
				text : 'Čas na stranu testu'
			},

			legend : {
				enabled : false
			},

			xAxis : {
				categories : [ <c:forEach items="${multipleTimeSpentData}" var="timeSpentData">'${timeSpentData.id}',</c:forEach><c:if test="${not empty fNorm}">'norma'</c:if> ],
				title : {
					text : 'ID Bourdonova testu.'
				}
			},

			yAxis : {
				title : {
					text : 'Strávený čas [ms]'
				},
			},
			
				series : [ {
					name : 'Observations',
					data : [ <c:forEach items="${multipleTimeSpentData}" var="timeSpentData">[ ${timeSpentData.minimum}, ${timeSpentData.lowerQuartile}, ${timeSpentData.median}, ${timeSpentData.upperQuartile}, ${timeSpentData.maximum} ],</c:forEach>
					<c:if test="${not empty fNorm}">[ ${tNorm.minimum}, 			${tNorm.lowerQuartile}, 			${tNorm.median},			${tNorm.upperQuartile},				${tNorm.maximum} 	],</c:if> ],
					tooltip : {
						headerFormat : '<em>Experiment No {point.key}</em><br/>'
					}
				} ]
			});
		});
</script>


<%@include file="/WEB-INF/layout/footer-wo-js.jsp"%>