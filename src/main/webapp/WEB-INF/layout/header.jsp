<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
	
<c:url var="postLoginUrl" value="/j_spring_security_check" /> 
<c:url var="logoutUrl" value="/j_spring_security_logout" />

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.ico">
<title>Psychodiagnostika pozornosti</title>
<!-- Bootstrap core CSS -->

<link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="${pageContext.request.contextPath}/resources/custom-css/mainsite.css" rel="stylesheet">
</head>
<body>

<div class="container">
	<nav class="navbar navbar-default" role="navigation">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span><span class="icon-bar"></span><span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="${pageContext.request.contextPath}">Psychodiagnostika
				pozornosti</a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="${pageContext.request.contextPath}/overview"><span
						class="glyphicon glyphicon-home"></span>Přehled</a></li>
				<li class="dropdown"><a href="${pageContext.request.contextPath}/results/" class="dropdown-toggle"
					data-toggle="dropdown"><span
						class="glyphicon glyphicon-list-alt"></span>Výsledky testů<b
						class="caret"></b></a>
					<ul class="dropdown-menu">
					    <li class="dropdown-header">Seznam testů</li>
						<li><a href="${pageContext.request.contextPath}/results/">Všechny testy</a></li>
						<li class="divider"></li>
						<li class="dropdown-header">Testy</li>
						<li><a href="${pageContext.request.contextPath}/results/bourdon/">Bourdonův test</a></li>
					</ul></li>
				<li class="dropdown"><a href="${pageContext.request.contextPath}/respondents/" class="dropdown-toggle"
					data-toggle="dropdown"><span
						class="glyphicon glyphicon-list-alt"></span>Respondenti<b
						class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="${pageContext.request.contextPath}/respondent/">Seznam respondentů</a></li>
						<li><a href="${pageContext.request.contextPath}/respondent/add">Přidat respondenta</a></li>
					</ul></li>
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown"><span
						class="glyphicon glyphicon-list-alt"></span>Statistika<b
						class="caret"></b></a>
					
					<ul class="dropdown-menu">
						<li class="dropdown-header">Bourdonův test</li>
						<li><a href="${pageContext.request.contextPath}/statistics/bourdon/list/">Exploratorní analýza</a></li>
					</ul></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span><sec:authentication property="principal.username" /><b
						class="caret"></b></a>
					<ul class="dropdown-menu">
<!-- 						<li><a href="#"><span class="glyphicon glyphicon-user"></span>Profil</a></li> -->
<!-- 						<li><a href="#"><span class="glyphicon glyphicon-cog"></span>Nastavení</a></li> -->
<!-- 						<li class="divider"></li> -->
						<li><a href="${logoutUrl}"><span class="glyphicon glyphicon-off"></span>Odhlášení</a></li>
					</ul></li>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</nav>