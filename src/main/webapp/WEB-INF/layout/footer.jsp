<%@ page pageEncoding="UTF-8" language="java" contentType="text/html; charset=UTF-8"%>

	<!-- Site footer -->
		<div class="footer">
			<p>&copy; Psychodiagnostika pozornosti 2014 - Jiří Šlachta</p>
		</div>

	</div>
	<!-- /container -->
	<!-- Bootstrap core JavaScript ================================================== -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</html>