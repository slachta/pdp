package eu.slachta.pdp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import eu.slachta.pdp.model.server.User;


public interface UserRepository extends JpaRepository<User, Integer> {

	@Query("select u from User u WHERE u.enabled = 1 ")
	public List<User> findAllactiveUsers();

	@Query("select u from User u WHERE u.username = :username ")
	public User findByUsername(@Param("username")String username);
	
	@Query("select u from User u left join fetch u.authorities WHERE u.username = :username ")
	public User findByUsernameFetchAuthorities(@Param("username")String username);
	
	@Query("select u from User u left join fetch u.authorities")
	public List<User> findAllFetchAuthorities();
	
	@Query("select u from User u left join fetch u.authorities WHERE u.id = :id ")
	public User findByIdFetchAuthorities(@Param("id")Integer id);
	
}
