<%@ page pageEncoding="UTF-8" language="java"
	contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/layout/header.jsp"%>

<div class="panel panel-default">
	<!-- Default panel contents -->
	<div class="panel-heading">Informace pro uživatele mobilní aplikace a interního API</div>
	<div class="panel-body">
		<h4>Informace pro připojení klienta BourdonMobile</h4>
		<p>Pro připojení použijte URI: ${fn:escapeXml(qrCodeURI)}</p>
		
		
		<p>Nebo použijte následující QR kód a načtěte jej přímo z aplikace:</p>
		<img src="http://qrfree.kaywa.com/?l=1&s=8&d=uri%3D${fn:escapeXml(qrCodeURI)}" alt="user" class="img-thumbnail">
	</div>
</div>

<div class="panel panel-default">
	<!-- Default panel contents -->
	<div class="panel-heading">Posledních 5 Bourdonových testů</div>
	<div class="panel-body">
		<p>Tato tabulka obsahuje naposledy přidané testy do databáze.</p>
	</div>

	<c:if test="${not empty tests}">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>ID testu</th>
					<th>Respondent</th>
					<th>Typ testu</th>
					<th>API klíč</th>
					<th>Datum testu</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${tests}" var="test">
					<tr>
						<td>${fn:escapeXml(test.id)}</td>
						<td>${fn:escapeXml(test.respondent.id)}</td>
						<td>${fn:escapeXml(test.testtype.name)}</td>
						<td>${fn:escapeXml(fn:substring(test.apiKey,0,20))}...</td>
						<td>${fn:escapeXml(test.date)}</td>
						<td><a
							href="${pageContext.request.contextPath}/results/${fn:escapeXml(test.testtype.prefix)}/test/${fn:escapeXml(test.id)}"
							class="btn btn-primary btn-xs">Zobrazit test</a></td>
					</tr>
				</c:forEach>
		</table>
	</c:if>
	<c:if test="${empty tests}">
		<div class="panel-body">
			<p>Testy nenalezeny.</p>
		</div>
	</c:if>
</div>

<div class="panel panel-default">
	<!-- Default panel contents -->
	<div class="panel-heading">Posledních 5 přidaných respondentů</div>
	<div class="panel-body">
		<p>Tato tabulka obsahuje naposledy přidané respondenty do
			databáze.</p>
	</div>

	<c:if test="${not empty respondents}">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>ID respondenta</th>
					<th>Identifikátor respondenta</th>
					<th>Jméno a příjmení</th>
					<th>Věk</th>
					<th>Pohlaví</th>
					<th>Vzdělání</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${respondents}" var="respondent">
					<tr>
						<td>${fn:escapeXml(respondent.id)}</td>
						<td>${fn:escapeXml(respondent.respondentKey)}</td>
						<td>${fn:escapeXml(respondent.name)}
							${fn:escapeXml(respondent.surname)}</td>
						<td>${fn:escapeXml(respondent.age)}</td>
						<td>${fn:escapeXml(respondent.sex)}</td>
						<td>${fn:escapeXml(respondent.educationLevel)}</td>
						<td><a
							href="${pageContext.request.contextPath}/respondent/${fn:escapeXml(respondent.id)}"
							class="btn btn-primary btn-xs">Zobrazit respondenta</a></td>
					</tr>
				</c:forEach>
		</table>
	</c:if>
	<c:if test="${empty respondents}">
		<div class="panel-body">
			<p>Respondenti nenalezeni.</p>
		</div>
	</c:if>
</div>

<%@include file="/WEB-INF/layout/footer.jsp"%>