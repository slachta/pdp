<%@ page pageEncoding="UTF-8" language="java"
	contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/layout/header.jsp"%>

<div class="row row-offcanvas row-offcanvas-right">
	<div class="col-xs-12 col-sm-9">
		<div class="panel panel-default">
			<!-- Default panel contents -->
			<div class="panel-heading">Sumarizovaná data</div>

			<div class="panel-body">
				<dl class="dl-horizontal">
					<dt>Počet testů:</dt>
					<dd>${fn:escapeXml(numberOfTests)}</dd>
					<dt>Property 1:</dt>
					<dd>Value 1</dd>
					<dt>Property 2:</dt>
					<dd>Value 2</dd>
					<dt>Property 3:</dt>
					<dd>Value 3</dd>
				</dl>
			</div>
		</div>

		<div class="panel panel-default">
			<!-- Default panel contents -->
			<div class="panel-heading">Posledních 15 testů</div>
			<div class="panel-body">
				<p>Tato tabulka obsahuje naposledy přidané testy do databáze.</p>
			</div>

			<c:if test="${not empty tests}">
				<table class="table table-hover row-clickable">
					<thead>
						<tr>
							<th>ID testu</th>
							<th>Respondent</th>
							<th>API klíč</th>
							<th>Datum testu</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${tests}" var="test">
							<tr>
								<td>${fn:escapeXml(test.id)}</td>
								<td>${fn:escapeXml(test.respondent.id)}</td>
								<td>${fn:escapeXml(fn:substring(test.apiKey,0,20))}...</td>
								<td>${fn:escapeXml(test.date)}</td>
								<td><a
									href="${pageContext.request.contextPath}/results/bourdon/test/${fn:escapeXml(test.id)}"
									class="btn btn-primary btn-xs">Zobrazit test</a></td>
							</tr>
						</c:forEach>
				</table>
			</c:if>
			<c:if test="${empty tests}">
				<div class="panel-body">
					<p>Testy nenalezeny.</p>
				</div>
			</c:if>
		</div>
	</div>

	<!--/span-->
	<div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar"
		role="navigation">
		<div class="panel panel-default">
			<!-- Default panel contents -->
			<div class="panel-heading">Bourdonův test</div>
			<nav class="nav-sidebar">
				<ul class="nav">
					<li><a
						href="${pageContext.request.contextPath}/results/bourdon/overview">Přehled
							a informace o testech</a></li>
					<li><a
						href="${pageContext.request.contextPath}/results/bourdon/all">Všechny
							testy</a></li>
					<li><a href="#">Hledání testů</a></li>
				</ul>
			</nav>
		</div>
	</div>
	<!--/span-->
</div>

<%@include file="/WEB-INF/layout/footer.jsp"%>