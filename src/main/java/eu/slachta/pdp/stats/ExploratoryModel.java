package eu.slachta.pdp.stats;

public class ExploratoryModel {
	private int id;
	private int size;
	private Double median;
	private Double mean;
	private Double minimum;
	private Double maximum;
	private Double lowerQuartile;
	private Double upperQuartile;
	private Double IQR;
	
	public ExploratoryModel() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the size
	 */
	public int getSize() {
		return size;
	}

	/**
	 * @param size the size to set
	 */
	public void setSize(int size) {
		this.size = size;
	}

	/**
	 * @return the mean
	 */
	public Double getMean() {
		return mean;
	}

	/**
	 * @param mean the mean to set
	 */
	public void setMean(Double mean) {
		this.mean = mean;
	}

	/**
	 * @return the minimum
	 */
	public Double getMinimum() {
		return minimum;
	}

	/**
	 * @param minimum the minimum to set
	 */
	public void setMinimum(Double minimum) {
		this.minimum = minimum;
	}

	/**
	 * @return the maximum
	 */
	public Double getMaximum() {
		return maximum;
	}

	/**
	 * @param maximum the maximum to set
	 */
	public void setMaximum(Double maximum) {
		this.maximum = maximum;
	}

	/**
	 * @return the lowerQuartile
	 */
	public Double getLowerQuartile() {
		return lowerQuartile;
	}

	/**
	 * @param lowerQuartile the lowerQuartile to set
	 */
	public void setLowerQuartile(Double lowerQuartile) {
		this.lowerQuartile = lowerQuartile;
	}

	/**
	 * @return the upperQuartile
	 */
	public Double getUpperQuartile() {
		return upperQuartile;
	}

	/**
	 * @param upperQuartile the upperQuartile to set
	 */
	public void setUpperQuartile(Double upperQuartile) {
		this.upperQuartile = upperQuartile;
	}

	/**
	 * @return the iQR
	 */
	public Double getIQR() {
		return IQR;
	}

	/**
	 * @param iQR the iQR to set
	 */
	public void setIQR(Double iQR) {
		IQR = iQR;
	}

	/**
	 * @return the median
	 */
	public Double getMedian() {
		return median;
	}

	/**
	 * @param median the median to set
	 */
	public void setMedian(Double median) {
		this.median = median;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

}
