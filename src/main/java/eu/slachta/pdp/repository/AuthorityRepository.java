package eu.slachta.pdp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import eu.slachta.pdp.model.server.Authority;
import eu.slachta.pdp.model.server.User;

public interface AuthorityRepository extends JpaRepository<Authority, Integer>{
	@Query("select a from Authority a where a.user = :user")
	public List<Authority> findAllByUser(@Param("user")User user);
	
	@Query("select a from Authority a where a.user = :user")
	public Authority findUserAuthority(@Param("user")User user);

}
