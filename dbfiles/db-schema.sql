SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `pdpdb` ;
CREATE SCHEMA IF NOT EXISTS `pdpdb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `pdpdb` ;

-- -----------------------------------------------------
-- Table `pdpdb`.`respondent`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pdpdb`.`respondent` ;

CREATE TABLE IF NOT EXISTS `pdpdb`.`respondent` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `respondentKey` VARCHAR(19) NOT NULL DEFAULT '0000-0000-0000-0000',
  `name` VARCHAR(45) NOT NULL DEFAULT 'anonymous',
  `surname` VARCHAR(45) NOT NULL DEFAULT 'anonymous',
  `age` INT NOT NULL DEFAULT -1,
  `sex` INT NOT NULL DEFAULT -1,
  `educationLevel` INT NOT NULL DEFAULT -1,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pdpdb`.`bourdonTest`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pdpdb`.`bourdonTest` ;

CREATE TABLE IF NOT EXISTS `pdpdb`.`bourdonTest` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `respondentId` INT UNSIGNED NOT NULL,
  `apiKey` VARCHAR(19) NOT NULL,
  `imagesPerPage` INT NOT NULL,
  `timePerPage` INT NOT NULL,
  `pages` INT NOT NULL,
  `date` DATETIME NOT NULL,
  `note` VARCHAR(45) NOT NULL,
  `resolution` VARCHAR(45) NOT NULL,
  `dpi` INT NOT NULL,
  `orientation` INT NOT NULL,
  `cellSize` INT NOT NULL,
  `refImg1Id` INT NOT NULL,
  `refImg2Id` INT NOT NULL,
  `refImg3Id` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `bourdonTestcol_UNIQUE` (`id` ASC),
  INDEX `fk_respondent_id_idx` (`respondentId` ASC),
  CONSTRAINT `fk_respondent_id`
    FOREIGN KEY (`respondentId`)
    REFERENCES `pdpdb`.`respondent` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pdpdb`.`bourdonTestPage`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pdpdb`.`bourdonTestPage` ;

CREATE TABLE IF NOT EXISTS `pdpdb`.`bourdonTestPage` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `testId` INT UNSIGNED NOT NULL,
  `pageNumber` INT NOT NULL,
  `wrongSetCount` INT NOT NULL,
  `rightSetCount` INT NOT NULL,
  `wrongAllCount` INT NOT NULL,
  `rightAllCount` INT NOT NULL,
  `timeSpent` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_bt_testId_idx` (`testId` ASC),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  CONSTRAINT `fk_bt_testId`
    FOREIGN KEY (`testId`)
    REFERENCES `pdpdb`.`bourdonTest` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
