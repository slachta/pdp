<%@ page pageEncoding="UTF-8" language="java"
	contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/layout/header.jsp"%>

<div class="row row-offcanvas row-offcanvas-right">
	<div class="col-xs-12 col-sm-9">
		<div class="panel panel-default">
			<!-- Default panel contents -->
			<div class="panel-heading">Informace o uživateli</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-3">
						<img
							src="http://qrfree.kaywa.com/?l=1&s=8&d=userid%3D${fn:escapeXml(respondent.respondentKey)}"
							alt="user" class="img-thumbnail">
					</div>
					<div class="col-sm-3">
						<dl class="dl-horizontal">
							<dt>Jméno:</dt>
							<dd>
								<c:choose>
									<c:when test="${empty respondent.name}">
        								anonymní
   									</c:when>
									<c:otherwise>
        								${fn:escapeXml(respondent.name)}
    								</c:otherwise>
								</c:choose>
							</dd>
							<dt>Příjmení:</dt>
							<dd>
								<c:choose>
									<c:when test="${empty respondent.surname}">
        								anonymní
   									</c:when>
									<c:otherwise>
        								${fn:escapeXml(respondent.surname)}
    								</c:otherwise>
								</c:choose>
							</dd>
							<dt>Věk:</dt>
							<dd>${fn:escapeXml(respondent.age)}</dd>
							<dt>Pohlaví:</dt>
							<dd>${fn:escapeXml(respondent.sex)}</dd>
							<dt>Vzdělání:</dt>
							<dd>${fn:escapeXml(respondent.educationLevel)}</dd>
							<dt>Identifikátor:</dt>
							<dd>${fn:escapeXml(respondent.respondentKey)}</dd>
						</dl>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">Testy respondenta</div>
			<c:if test="${not empty tests}">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>ID testu</th>
							<th>Typ testu</th>
							<th>API klíč</th>
							<th>Datum testu</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${tests}" var="test">
							<tr>
								<td>${fn:escapeXml(test.id)}</td>
								<td>${fn:escapeXml(test.testtype.name)}</td>
								<td>${fn:escapeXml(fn:substring(test.apiKey,0,20))}...</td>
								<td>${fn:escapeXml(test.date)}</td>
								<td><a
									href="${pageContext.request.contextPath}/results/${fn:escapeXml(test.testtype.prefix)}/test/${fn:escapeXml(test.id)}"
									class="btn btn-primary btn-xs">Zobrazit test</a> <a
									href="${pageContext.request.contextPath}/respondent/delete/${respondent.id}/deleteTest/${fn:escapeXml(test.id)}"
									class="btn btn-danger btn-xs">Smazat test</a></td>
							</tr>
						</c:forEach>
				</table>
			</c:if>
			<c:if test="${empty tests}">
				<div class="panel-body">
					<p>Testy nenalezeny.</p>
				</div>
			</c:if>
		</div>
	</div>
	<!--/span-->
	<div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar"
		role="navigation">
		<div class="panel panel-default">
			<div class="panel-heading">Respondenti</div>
			<nav class="nav-sidebar">
				<ul class="nav">
					<li><a href="${pageContext.request.contextPath}/respondent/">Seznam
							respondentů</a></li>
					<li><a href="#">Hledání</a></li>
				</ul>
			</nav>
		</div>
	</div>
	<!--/span-->
</div>

<%@include file="/WEB-INF/layout/footer.jsp"%>