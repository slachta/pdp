package eu.slachta.pdp.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import eu.slachta.pdp.model.server.Bourdontest;
import eu.slachta.pdp.model.server.Bourdontestpage;
import eu.slachta.pdp.service.BourdonTestPageService;
import eu.slachta.pdp.service.BourdonTestService;
import eu.slachta.pdp.service.RespondentService;

@Controller
public class BourdonResultsController {

	@Resource(name = "bourdonTestPageService")
	private BourdonTestPageService bourdonTestPageService;

	@Resource(name = "bourdonTestService")
	private BourdonTestService bourdonTestService;

	@Resource(name = "respondentService")
	private RespondentService respondentService;

	@RequestMapping(value = "/results/bourdon", method = RequestMethod.GET)
	public String redir(ModelMap model, Principal principal) {
		return "redirect:/results/bourdon/all";
	}

	@RequestMapping(value = "/results/bourdon/overview", method = RequestMethod.GET)
	public String printOverview(ModelMap model, Principal principal) {
		model.addAttribute("tests", bourdonTestService.findLastTestsForOverview(15));
		model.addAttribute("numberOfTests", bourdonTestService.getCount());

		return "bourdonOverview";
	}

	@RequestMapping(value = "/results/bourdon/all", method = RequestMethod.GET)
	public String printListOfAllTests(ModelMap model, Principal principal) {
		List<Bourdontest> bt = bourdonTestService.findAll();

		model.addAttribute("tests", bt);
		return "bourdonAllTests";
	}

	@RequestMapping("/results/bourdon/test/{id}")
	public String getTest(@PathVariable Integer id, ModelMap model) {
		if(bourdonTestService.exists(id)){
			Bourdontest bt = bourdonTestService.findById(id);
			ArrayList<Bourdontestpage> btps = bourdonTestPageService.findByTestId(id);

			model.addAttribute("test", bt);
			model.addAttribute("testPages", btps);

			passGraphsData(model, bt, btps);
		}
		
		return "bourdonTest";
	}
	
	@RequestMapping("/results/bourdon/delete/{id}")
	public String deleteTest(@PathVariable Integer id, ModelMap model) {
		Bourdontest bt = bourdonTestService.findById(id);
		
		if(bt != null){
			bourdonTestService.delete(id);
			
			List<Bourdontest> lbt = bourdonTestService.findAll();
			
			model.addAttribute("tests", lbt);
			model.addAttribute("id", id);
		}else{
			List<Bourdontest> lbt = bourdonTestService.findAll();

			model.addAttribute("tests", lbt);
			model.addAttribute("idn", id);
		}

		return "bourdonAllTests";
	}

	private void passGraphsData(ModelMap model, Bourdontest bt, ArrayList<Bourdontestpage> btps) {
		
		String fruitfulness = "\"Stránka testu,Počet správně zvolených obrázků,Počet správných obrázků\\n\"+\n";
		for (int i = 0; i < btps.size()-1; i++) {
			fruitfulness += "\"" + btps.get(i).getPageNumber() + ","
					+ btps.get(i).getRightSetCount() + ","
					+ btps.get(i).getRightAllCount() + "\\n\"+\n";
		}
		fruitfulness += "\"" + btps.get(btps.size()-1).getPageNumber() + ","
				+ btps.get(btps.size()-1).getRightSetCount() + ","
				+ btps.get(btps.size()-1).getRightAllCount() + "\"";

		model.addAttribute("fruitfulnessGraph", fruitfulness);

		String errorRate = "\"Stránka testu,Počet nesprávně zvolených obrázků,Počet nesprávných obrázků\\n\"+\n";
		for (int i = 0; i < btps.size()-1; i++) {
			errorRate += "\"" + btps.get(i).getPageNumber() + ","
					+ btps.get(i).getWrongSetCount() + ","
					+ btps.get(i).getWrongAllCount() + "\\n\"+\n";
		}
		errorRate += "\"" + btps.get(btps.size()-1).getPageNumber() + ","
				+ btps.get(btps.size()-1).getWrongSetCount() + ","
				+ btps.get(btps.size()-1).getWrongAllCount() + "\"";

		model.addAttribute("errorRateGraph", errorRate);

		String time = "\"Stránka testu, Strávený čas na stranu testu, Maximální čas na stranu testu\\n\"+\n";
		for (int i = 0; i < btps.size(); i++) {
			time += "\"" + btps.get(i).getPageNumber() + ","
					+ btps.get(i).getTimeSpent() + "," + bt.getTimePerPage()
					+ "\\n\"+\n";
		}
		time += "\"" + btps.get(btps.size()-1).getPageNumber() + ","
				+ btps.get(btps.size()-1).getTimeSpent() + "," + bt.getTimePerPage()
				+ "\"";

		model.addAttribute("timeGraph", time);

		String cumulative = "\"Stránka testu, Strávený čas na stranu testu, Správně zvolené obrázky, Nesprávně zvolené obrázky\\n\"+\n";
		for (int i = 0; i < btps.size()-1; i++) {
			cumulative += "\"" + btps.get(i).getPageNumber() + ","
					+ (int)((bt.getTimePerPage()!=0)?(((double)(btps.get(i).getTimeSpent())/(double)bt.getTimePerPage())*100):0) + ","
					+ (int)((btps.get(i).getRightAllCount()!=0)?(((double)btps.get(i).getRightSetCount()/(double)btps.get(i).getRightAllCount())*100):0) + ","
					+ (int)((btps.get(i).getWrongAllCount()!=0)?(((double)btps.get(i).getWrongSetCount()/(double)btps.get(i).getWrongAllCount())*100):0) + "\\n\"+\n";
		}
		cumulative += "\"" + btps.get(btps.size()-1).getPageNumber() + ","
				+ (int)((bt.getTimePerPage()!=0)?(((double)(btps.get(btps.size()-1).getTimeSpent())/(double)bt.getTimePerPage())*100):0) + ","
				+ (int)((btps.get(btps.size()-1).getRightAllCount()!=0)?(((double)btps.get(btps.size()-1).getRightSetCount()/(double)btps.get(btps.size()-1).getRightAllCount())*100):0) + ","
				+ (int)((btps.get(btps.size()-1).getWrongAllCount()!=0)?(((double)btps.get(btps.size()-1).getWrongSetCount()/(double)btps.get(btps.size()-1).getWrongAllCount())*100):0) + "\"";

		model.addAttribute("cumulativeGraph", cumulative);
	}
}
