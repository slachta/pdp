package eu.slachta.pdp.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import eu.slachta.pdp.model.server.Respondent;
import eu.slachta.pdp.repository.RespondentRepository;

/**
 * 
 * 
 * @author Jiri Slachta
 */

@Service
@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
public class RespondentService {

	@Resource
	RespondentRepository respondentDao;

	public void createRespondent(Respondent respondent) {
		respondentDao.save(respondent);
	}

	public void deleteRespondent(int testId) {
		respondentDao.delete(testId);
	}

	public Respondent findById(int testId) {
		return respondentDao.findOne(testId);
	}
	
	public ArrayList<Respondent> findByRespondentKey(String key){
		return respondentDao.findByRespondentKey(key);
	}

	public List<Respondent> findAll() {
		return respondentDao.findAll();
	}
	
	public ArrayList<Respondent> findLastAddedRespondents(Integer limit){
		ArrayList<Respondent> r = respondentDao.findLastAddedRespondents(new PageRequest(0, limit));
		
		return r;
	}
}
