package eu.slachta.pdp.model.server;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the respondent database table.
 * 
 */
@Entity
@Table(name="respondent")
@NamedQuery(name="Respondent.findAll", query="SELECT r FROM Respondent r")
public class Respondent implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int id;

	@Column(nullable=false)
	private int age;

	@Column(nullable=false)
	private int educationLevel;

	@Column(nullable=false, length=45)
	private String name;

	@Column(nullable=false, length=19)
	private String respondentKey;

	@Column(nullable=false)
	private int sex;

	@Column(nullable=false, length=45)
	private String surname;

	//bi-directional many-to-one association to Bourdontest
	@OneToMany(mappedBy="respondent")
	private List<Bourdontest> bourdontests;

	public Respondent() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getEducationLevel() {
		return this.educationLevel;
	}

	public void setEducationLevel(int educationLevel) {
		this.educationLevel = educationLevel;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRespondentKey() {
		return this.respondentKey;
	}

	public void setRespondentKey(String respondentKey) {
		this.respondentKey = respondentKey;
	}

	public int getSex() {
		return this.sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public String getSurname() {
		return this.surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public List<Bourdontest> getBourdontests() {
		return this.bourdontests;
	}

	public void setBourdontests(List<Bourdontest> bourdontests) {
		this.bourdontests = bourdontests;
	}

	public Bourdontest addBourdontest(Bourdontest bourdontest) {
		getBourdontests().add(bourdontest);
		bourdontest.setRespondent(this);

		return bourdontest;
	}

	public Bourdontest removeBourdontest(Bourdontest bourdontest) {
		getBourdontests().remove(bourdontest);
		bourdontest.setRespondent(null);

		return bourdontest;
	}

}