package eu.slachta.pdp.support;

public interface Support {

	public void logFormManipulation(String errorInfo);

	public void logDateIntegrityViolation(String action, String usedValue,
			String infoText);

	public void logUserLogin(String username, String role, String firstTime);

	public void logUnauthorizedAccessAttempt();

	public void logoutInfo(String username);

	public void logDatabaseTransaction(String action, String id);

	public void logDatabaseTransactionError(String action, Throwable cause);

	public void logLoginWithNoOrganization(String username);
}