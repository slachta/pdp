package eu.slachta.pdp.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;

import eu.slachta.pdp.model.bm.BourdonMobileTest;
import eu.slachta.pdp.model.bm.BourdonMobileTestInformation;
import eu.slachta.pdp.model.bm.BourdonMobileTestPageData;
import eu.slachta.pdp.model.server.Bourdontest;
import eu.slachta.pdp.model.server.Bourdontestpage;
import eu.slachta.pdp.model.server.Respondent;
import eu.slachta.pdp.model.server.Testtype;
import eu.slachta.pdp.repository.BourdonTestPagesRepository;
import eu.slachta.pdp.repository.BourdonTestRepository;
import eu.slachta.pdp.repository.RespondentRepository;
import eu.slachta.pdp.repository.TestTypeRepository;

/**
 * Pridavani testu se strankami testu a uzivateli s tim, ze pokud jiz uzivatel
 * existuje, at se paruje test k existujicimu uzivateli (ale nesmi byt anonym)
 * 
 * @author Jiri Slachta
 */

@Service
@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
public class BourdonTestService {
	@Resource
	BourdonTestRepository bourdonTestDao;

	@Resource
	BourdonTestPagesRepository bourdonTestPagesDao;

	@Resource
	RespondentRepository respondentDao;

	@Resource
	TestTypeRepository testTypeDao;

	private Logger logger = LoggerFactory.getLogger(BourdonTestService.class);

	/**
	 * Adds a test into database. If the respondent is not anonymous, it tries
	 * to pair the test with the corresponding respondent.
	 * 
	 * @param respondent
	 * @param test
	 * @param testPages
	 */

	public void createTest(Respondent respondent, Bourdontest test,
			ArrayList<Bourdontestpage> testPages) {
		ArrayList<Bourdontest> tests = bourdonTestDao.findTestByApiKeyAndDate(
				test.getApiKey(), test.getDate());

		Testtype testType = testTypeDao.findOne(1);
		if (testType == null) {
			testType = new Testtype();
			testType.setId(1);
			testType.setName("Bourdonův test");
			testType.setPrefix("bourdon");
			testTypeDao.save(testType);
		}

		if (tests.size() == 0) {
			if (respondent.getRespondentKey().equals("0000-0000-0000-0000")) {
				respondent = respondentDao.save(respondent);
				logger.info("Anonymous respondent added.");
			} else {
				ArrayList<Respondent> respondents = respondentDao
						.findByRespondentKey(respondent.getRespondentKey());

				if (respondents.size() == 0) {
					respondent = respondentDao.save(respondent);
					logger.info("Respondent with ID " + respondent.getId()
							+ " added");
				} else {
					respondent = respondents.get(0);
					logger.info("Respondent exists, pairing test to existing respondent.");
				}
			}

			test.setRespondent(respondent);
			test = bourdonTestDao.save(test);
			logger.info("Bourdon test added to corresponding respondent ID "
					+ respondent.getId());

			test.setBourdontestpages(testPages);
			for (int i = 0; i < testPages.size(); i++) {
				testPages.get(i).setBourdontest(test);
			}

			bourdonTestPagesDao.save(testPages);
			logger.info("Bourdon test pages added to corresponding test ID "
					+ test.getId());
		} else {
			logger.info("Bourdon test for given API key and date is already in database.");
		}
	}

	public String getTestsInJSON() {
		List<Bourdontest> tests = bourdonTestDao.findAll();

		if (tests.size() > 0) {
			Gson gson = new Gson();

			BourdonMobileTest bt;
			BourdonMobileTestInformation t;
			BourdonMobileTest[] bts = new BourdonMobileTest[tests.size()];

			for (int i = 0; i < bts.length; i++) {
				t = new BourdonMobileTestInformation(tests.get(i).getId(),
						tests.get(i).getRespondent().getRespondentKey(), tests
								.get(i).getImagesPerPage(), tests.get(i)
								.getTimePerPage(), tests.get(i).getPages(),
						tests.get(i).getDate().getTime(), tests.get(i)
								.getRespondent().getAge(), tests.get(i)
								.getRespondent().getSex(), tests.get(i)
								.getRespondent().getEducationLevel(), tests
								.get(i).getNote(),
						tests.get(i).getResolution(), tests.get(i).getDpi(),
						tests.get(i).getOrientation(), tests.get(i)
								.getCellSize(), tests.get(i).getRefImg1Id(),
						tests.get(i).getRefImg2Id(), tests.get(i)
								.getRefImg3Id());

				bt = new BourdonMobileTest(0, t, constructPageData(tests.get(i)
						.getBourdontestpages()));
				bts[i] = bt;
			}

			return gson.toJson(bts);
		} else {
			return "";
		}
	}

	public String getTestInJSON(int id) {
		Bourdontest test = bourdonTestDao.findOne(id);

		if (test != null) {
			Gson gson = new Gson();

			BourdonMobileTest bt;
			BourdonMobileTestInformation t;
			BourdonMobileTest[] bts = new BourdonMobileTest[1];

			t = new BourdonMobileTestInformation(test.getId(), test
					.getRespondent().getRespondentKey(),
					test.getImagesPerPage(), test.getTimePerPage(),
					test.getPages(), test.getDate().getTime(), test
							.getRespondent().getAge(), test.getRespondent()
							.getSex(),
					test.getRespondent().getEducationLevel(), test.getNote(),
					test.getResolution(), test.getDpi(), test.getOrientation(),
					test.getCellSize(), test.getRefImg1Id(),
					test.getRefImg2Id(), test.getRefImg3Id());

			bt = new BourdonMobileTest(0, t,
					constructPageData(test.getBourdontestpages()));
			bts[0] = bt;

			return gson.toJson(bts);
		} else {
			return "";
		}
	}

	private ArrayList<BourdonMobileTestPageData> constructPageData(
			List<Bourdontestpage> testPages) {
		ArrayList<BourdonMobileTestPageData> pageData = new ArrayList<BourdonMobileTestPageData>();
		List<Bourdontestpage> tp = testPages;
		for (Bourdontestpage btp : tp) {
			pageData.add(new BourdonMobileTestPageData(btp.getPageNumber(), btp
					.getId(), btp.getPageNumber(), btp.getWrongSetCount(), btp
					.getRightSetCount(), btp.getWrongAllCount(), btp
					.getRightAllCount(), btp.getTimeSpent()));
		}

		return pageData;
	}

	public ArrayList<Bourdontest> findLastTestsForOverview(Integer limit) {
		ArrayList<Bourdontest> bt = bourdonTestDao
				.findLastTests(new PageRequest(0, limit));

		for (int i = 0; i < bt.size(); i++) {
			Testtype tt = bt.get(i).getTesttype();
			Bourdontest test = bt.get(i);
			test.setTesttype(tt);
			bt.set(i, test);
		}

		return bt;
	}

	public ArrayList<Bourdontest> findTestsByRespondentId(Integer id) {
		ArrayList<Bourdontest> bt = null;
		
		existsByRespondentId(id);
		
		bt = bourdonTestDao.findTestByRespondentId(id);

		for (int i = 0; i < bt.size(); i++) {
			Testtype tt = bt.get(i).getTesttype();
			Bourdontest test = bt.get(i);
			test.setTesttype(tt);
			bt.set(i, test);
		}

		return bt;
	}

	public void delete(int testId) {
		logger.info("Deleting test by ID: " + testId);
		bourdonTestDao.delete(testId);
	}

	public Bourdontest findById(int testId) {
		logger.info("Finding test by ID: " + testId);
		return bourdonTestDao.findOne(testId);
	}

	public ArrayList<Bourdontest> findByApiKey(String apiKey) {
		return bourdonTestDao.findTestByApiKey(apiKey);
	}

	public List<Bourdontest> findAll() {
		return bourdonTestDao.findAll();
	}

	public Long getCount() {
		return bourdonTestDao.count();
	}

	public Boolean existsByRespondentId(Integer id) {
		if(bourdonTestDao.existsByRespondentId(id)>1){
			return true;
		}else{
			return false;
		}
	}

	public boolean exists(Integer testId) {
		return bourdonTestDao.exists(testId);
	}
}
