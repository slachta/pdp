package eu.slachta.pdp.controller;

import java.security.Principal;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import eu.slachta.pdp.service.BourdonTestService;
import eu.slachta.pdp.service.RespondentService;

@Controller
public class OverviewController {
	
	@Resource(name = "bourdonTestService")
	private BourdonTestService bourdonTestService;
	
	@Resource(name = "respondentService")
	private RespondentService respondentService;
	
	@RequestMapping(value="/overview", method = RequestMethod.GET)
	public String printOverview(ModelMap model, Principal principal, HttpServletRequest request) {
		String uri = "http://"+request.getServerName()+":"+request.getLocalPort()+""+request.getContextPath();

		model.addAttribute("qrCodeURI", uri);
		model.addAttribute("tests", bourdonTestService.findLastTestsForOverview(5));
		model.addAttribute("respondents", respondentService.findLastAddedRespondents(5));
		return "overview";
	}
}
