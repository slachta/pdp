package eu.slachta.pdp.form;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.validation.Errors;

public class NewRespondentForm {
	private String name;
	private String surname;
	private int age;
	private int educationLevel;
	private int sex;
	@NotEmpty
	@Length(min = 19, max = 19)
	private String respondentKey;
	
	public void validate(Errors errors){
		
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}
	/**
	 * @param surname the surname to set
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}
	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}
	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}
	/**
	 * @return the educationLevel
	 */
	public int getEducationLevel() {
		return educationLevel;
	}
	/**
	 * @param educationLevel the educationLevel to set
	 */
	public void setEducationLevel(int educationLevel) {
		this.educationLevel = educationLevel;
	}
	/**
	 * @return the sex
	 */
	public int getSex() {
		return sex;
	}
	/**
	 * @param sex the sex to set
	 */
	public void setSex(int sex) {
		this.sex = sex;
	}
	/**
	 * @return the respondentKey
	 */
	public String getRespondentKey() {
		return respondentKey;
	}
	/**
	 * @param respondentKey the respondentKey to set
	 */
	public void setRespondentKey(String respondentKey) {
		this.respondentKey = respondentKey;
	}
	
}
