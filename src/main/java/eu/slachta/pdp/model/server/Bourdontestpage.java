package eu.slachta.pdp.model.server;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the bourdontestpage database table.
 * 
 */
@Entity
@Table(name="bourdontestpage")
@NamedQuery(name="Bourdontestpage.findAll", query="SELECT b FROM Bourdontestpage b")
public class Bourdontestpage implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int id;

	@Column(nullable=false)
	private int pageNumber;

	@Column(nullable=false)
	private int rightAllCount;

	@Column(nullable=false)
	private int rightSetCount;

	@Column(nullable=false)
	private int timeSpent;

	@Column(nullable=false)
	private int wrongAllCount;

	@Column(nullable=false)
	private int wrongSetCount;

	//bi-directional many-to-one association to Bourdontest
	@ManyToOne
	@JoinColumn(name="testId", nullable=false)
	private Bourdontest bourdontest;

	public Bourdontestpage() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPageNumber() {
		return this.pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getRightAllCount() {
		return this.rightAllCount;
	}

	public void setRightAllCount(int rightAllCount) {
		this.rightAllCount = rightAllCount;
	}

	public int getRightSetCount() {
		return this.rightSetCount;
	}

	public void setRightSetCount(int rightSetCount) {
		this.rightSetCount = rightSetCount;
	}

	public int getTimeSpent() {
		return this.timeSpent;
	}

	public void setTimeSpent(int timeSpent) {
		this.timeSpent = timeSpent;
	}

	public int getWrongAllCount() {
		return this.wrongAllCount;
	}

	public void setWrongAllCount(int wrongAllCount) {
		this.wrongAllCount = wrongAllCount;
	}

	public int getWrongSetCount() {
		return this.wrongSetCount;
	}

	public void setWrongSetCount(int wrongSetCount) {
		this.wrongSetCount = wrongSetCount;
	}

	public Bourdontest getBourdontest() {
		return this.bourdontest;
	}

	public void setBourdontest(Bourdontest bourdontest) {
		this.bourdontest = bourdontest;
	}

}