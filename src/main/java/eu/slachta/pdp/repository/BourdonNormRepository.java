package eu.slachta.pdp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import eu.slachta.pdp.model.server.Bourdonnorm;

public interface BourdonNormRepository extends JpaRepository<Bourdonnorm, Integer>{
	
	@Query("SELECT n FROM Bourdonnorm n WHERE n.name = :name ")
	public Bourdonnorm findByName(@Param("name") String name);

}
