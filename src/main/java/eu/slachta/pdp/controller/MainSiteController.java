package eu.slachta.pdp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainSiteController {

	@RequestMapping("/403")
	public String forbidden(Model model) {
		return "../errors/403";
	}

	@RequestMapping("/404")
	public String notFound(Model model) {
		return "../errors/404";
	}

}
