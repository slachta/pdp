<%@ page pageEncoding="UTF-8" language="java"
	contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/layout/header.jsp"%>

<div class="panel panel-default">
	<!-- Default panel contents -->
	<div class="panel-heading">Seznam testů pro exploratorní analýzu</div>
	<!-- Table -->
	<c:if test="${not empty tests}">
		<div class="panel-body">
			<form:form action="${pageContext.request.contextPath}/statistics/bourdon/multipleexploratory" method="POST" commandName="bourdonStatsTestChoices">
				<table class="table table-hover">
					<thead>
						<tr>
							<th></th>
							<th>ID testu</th>
							<th>Respondent</th>
							<th>API klíč</th>
							<th>Datum testu</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${tests}" var="test">
							<tr>
								<td><form:checkbox path="ids" value="${fn:escapeXml(test.id)}" /></td>
								<td>${fn:escapeXml(test.id)}</td>
								<td>${fn:escapeXml(test.respondent.id)}</td>
								<td>${fn:escapeXml(fn:substring(test.apiKey,0,20))}...</td>
								<td>${fn:escapeXml(test.date)}</td>
								<td><a
									href="${pageContext.request.contextPath}/statistics/${fn:escapeXml(test.testtype.prefix)}/exploratory/${fn:escapeXml(test.id)}"
									class="btn btn-primary btn-xs">Exploratorní analýza</a></td>
							</tr>
						</c:forEach>
				</table>
				<div class="panel-footer">
					<button type="submit" class="btn btn-default" name="action" value="compare">Srovnání exploratorních analýz testů</button>
					<button type="submit" class="btn btn-default" name="action" value="norm">Stanovit normu z výběru testů</button>
					<button type="submit" class="btn btn-default" name="action" value="allnorm">Stanovit normu ze všech testů</button>
				</div>
			</form:form>
		</div>
	</c:if>
	<c:if test="${empty tests}">
		<div class="panel-body">
			<p>Testy nenalezeny.</p>
		</div>
	</c:if>
</div>


<%@include file="/WEB-INF/layout/footer.jsp"%>