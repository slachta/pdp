<%@ page pageEncoding="UTF-8" language="java"
	contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/layout/header.jsp"%>

<div class="row row-offcanvas row-offcanvas-right">
	<div class="col-xs-12 col-sm-9">
		<div class="panel panel-default">
			<!-- Default panel contents -->
			<div class="panel-heading">Seznam respondentů</div>
			<!-- Table -->
			<c:if test="${not empty id}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">&times;</button>
					Respondent s ID ${id} smazán.
				</div>
			</c:if>
			<c:if test="${not empty idn}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">&times;</button>
					Respondent s ID ${idn} nenalezen, nelze jej smazat.
				</div>
			</c:if>
			<c:if test="${not empty added}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">&times;</button>
					Respondent přidán.
				</div>
			</c:if>
			<c:if test="${not empty respondents}">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>ID respondenta</th>
							<th>Identifikátor respondenta</th>
							<th>Jméno a příjmení</th>
							<th>Věk</th>
							<th>Pohlaví</th>
							<th>Vzdělání</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${respondents}" var="respondent">
							<tr>
								<td>${fn:escapeXml(respondent.id)}</td>
								<td>${fn:escapeXml(respondent.respondentKey)}</td>
								<td><c:if test="${respondent.age != '0'}">
										${fn:escapeXml(respondent.name)} ${fn:escapeXml(respondent.surname)}
									</c:if>
									<c:if test="${respondent.age == '0'}">
											anonymní
									</c:if></td>
								<td>${fn:escapeXml(respondent.age)}</td>
								<td><c:if test="${respondent.age != '0'}">
										<c:if test="${respondent.sex == '0'}">
											muž
										</c:if>
										<c:if test="${respondent.sex == '1'}">
											žena
										</c:if>
									</c:if>
									<c:if test="${respondent.age == '0'}">
											anonymní
									</c:if>
									</td>
								<td>${fn:escapeXml(respondent.educationLevel)}</td>
								<td><a
									href="${pageContext.request.contextPath}/respondent/${fn:escapeXml(respondent.id)}"
									class="btn btn-primary btn-xs">Zobrazit</a> <a
									href="${pageContext.request.contextPath}/respondent/delete/${fn:escapeXml(respondent.id)}"
									class="btn btn-danger btn-xs">Smazat</a></td>
							</tr>
						</c:forEach>
				</table>
			</c:if>
			<c:if test="${empty respondents}">
				<div class="panel-body">
					<p>Respondenti nenalezeni.</p>
				</div>
			</c:if>
		</div>
	</div>
	<!--/span-->
	<!--/span-->
	<div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar"
		role="navigation">
		<div class="panel panel-default">
			<div class="panel-heading">Respondenti</div>
			<nav class="nav-sidebar">
				<ul class="nav">
					<li><a href="${pageContext.request.contextPath}/respondent/">Seznam respondentů</a></li>
					<li><a href="${pageContext.request.contextPath}/respondent/add">Přidat respondenta</a></li>
					<li><a href="#">Hledání</a></li>
				</ul>
			</nav>
		</div>
	</div>
	<!--/span-->
</div>


<%@include file="/WEB-INF/layout/footer.jsp"%>