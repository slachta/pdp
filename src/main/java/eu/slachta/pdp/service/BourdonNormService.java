package eu.slachta.pdp.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import eu.slachta.pdp.model.server.Bourdonnorm;
import eu.slachta.pdp.repository.BourdonNormRepository;

/**
 * 
 * 
 * @author Jiri Slachta
 */

@Service
@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
public class BourdonNormService {

	@Resource
	BourdonNormRepository bourdonNormDao;

	public void createNorm(Bourdonnorm norm) {
		bourdonNormDao.save(norm);
	}
	
	public void updateNorm(Bourdonnorm norm){
		bourdonNormDao.save(norm);
	}

	public void deleteNorm(int id) {
		bourdonNormDao.delete(id);
	}

	public Bourdonnorm findById(int testId) {
		return bourdonNormDao.findOne(testId);
	}
	
	public Bourdonnorm findByName(String name){
		return bourdonNormDao.findByName(name);
	}

	public List<Bourdonnorm> findAll() {
		return bourdonNormDao.findAll();
	}
}
