package eu.slachta.pdp.controller;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import eu.slachta.pdp.form.NewRespondentForm;
import eu.slachta.pdp.model.server.Bourdontest;
import eu.slachta.pdp.model.server.Respondent;
import eu.slachta.pdp.service.BourdonTestPageService;
import eu.slachta.pdp.service.BourdonTestService;
import eu.slachta.pdp.service.RespondentService;
import eu.slachta.pdp.service.TestTypeService;

@Controller
public class RespondentController {
	private static final Logger logger = LoggerFactory.getLogger(RespondentController.class);

	@Resource(name = "bourdonTestPageService")
	private BourdonTestPageService bourdonTestPageService;

	@Resource(name = "bourdonTestService")
	private BourdonTestService bourdonTestService;

	@Resource(name = "respondentService")
	private RespondentService respondentService;
	
	@Resource(name = "testTypeService")
	private TestTypeService testtypeService;
	
	@RequestMapping(value = "/respondent", method = RequestMethod.GET)
	public String redir(ModelMap model, Principal principal) {
		return "redirect:/respondent/all";
	}
	
	@RequestMapping(value = "/respondent/all", method = RequestMethod.GET)
	public String printListOfAllTests(ModelMap model, Principal principal) {
		List<Respondent> r = respondentService.findAll();

		model.addAttribute("respondents", r);
		return "respondentsAll";
	}

	@RequestMapping("/respondent/{id}")
	public String getTest(@PathVariable Integer id, ModelMap model) {
		Respondent r = respondentService.findById(id);
		model.addAttribute("respondent", r);
		
		List<Bourdontest> bt = bourdonTestService.findTestsByRespondentId(id);
		model.addAttribute("tests", bt);
		
		return "respondent";
	}
	
	@RequestMapping("/respondent/delete/{id}")
	public String deleteTest(@PathVariable Integer id, ModelMap model) {
		Respondent r = respondentService.findById(id);
		
		if(r != null){
			respondentService.deleteRespondent(id);
			
			List<Respondent> lr = respondentService.findAll();

			model.addAttribute("respondents", lr);
			model.addAttribute("id", id);
		}else{
			List<Respondent> lr = respondentService.findAll();

			model.addAttribute("respondents", lr);
			model.addAttribute("id", id);
		}

		return "respondentsAll";
	}
	
	@RequestMapping(value="/respondent/delete/{id}/deleteTest/{testId}")
	public String deleteRespondentsTest(@PathVariable Integer id, @PathVariable Integer testId, ModelMap model) {
		bourdonTestService.delete(testId);
		
		List<Bourdontest> bt = bourdonTestService.findTestsByRespondentId(id);
		model.addAttribute("tests", bt);
		
		Respondent r = respondentService.findById(id);
		model.addAttribute("respondent", r);

		return "respondentsAll";
	}
	
	@RequestMapping(value="/respondent/search")
	public String search(ModelMap model) {

		return "respondentsAll";
	}
	
	@RequestMapping(value="/respondent/add", method = RequestMethod.GET)
	public String getRespondentAdditionForm(
			ModelMap model, 
			@RequestParam(value="exists", required=false) Boolean exists, 
			@RequestParam(value="notValidUserKey", required=false) Boolean notValidUserKey
			) 
	{
		Map<Integer, String > sex = new HashMap<Integer, String>();
		sex.put(0, "Muž");
		sex.put(1, "Žena");

		model.addAttribute("sex", sex);
		
		Map<Integer, String > educationLevel = new HashMap<Integer, String>();
		educationLevel.put(0, "Základní vzdělání");
		educationLevel.put(1, "Střední vzdělání s výučním listem");
		educationLevel.put(2, "Střední vzdělání s maturitní zkouškou");
		educationLevel.put(3, "Vyšší odborné vzdělání nebo vysokoškolské vzdělání - bakalářský program");
		educationLevel.put(4, "Vysokoškolské vzdělání - magisterský program");
		educationLevel.put(5, "Vysokoškolské vzdělání - doktorský program");

		model.addAttribute("educationLevel", educationLevel);
		model.addAttribute("formData", new NewRespondentForm());
		model.addAttribute("exists", exists);
		model.addAttribute("notValidUserKey", notValidUserKey);	
		
		return "respondentAddForm";
	}
	
	@RequestMapping(value="/respondent/add", method = RequestMethod.POST)
	public String getRespondentAdd(ModelMap model, @Valid NewRespondentForm form) {
		if(isUserKeyValid(form.getRespondentKey())){
			if(form.getRespondentKey().equals("0000-0000-0000-0000")){
				logger.info("Respondent added");
				Respondent respondent = new Respondent();
				respondent.setAge(form.getAge());
				respondent.setEducationLevel(form.getEducationLevel());
				respondent.setName(form.getName());
				model.addAttribute("name", form.getName());
				respondent.setRespondentKey(form.getRespondentKey());
				respondent.setSex(form.getSex());
				respondent.setSurname(form.getSurname());
				respondentService.createRespondent(respondent);
			}else{
				if(respondentService.findByRespondentKey(form.getRespondentKey()).size()==0){
					logger.info("Respondent added");
					Respondent respondent = new Respondent();
					respondent.setAge(form.getAge());
					respondent.setEducationLevel(form.getEducationLevel());
					model.addAttribute("name", form.getName());
					respondent.setName(form.getName());
					respondent.setRespondentKey(form.getRespondentKey());
					respondent.setSex(form.getSex());
					respondent.setSurname(form.getSurname());
					respondentService.createRespondent(respondent);
				}else{
					logger.info("Respondent exists");
					model.addAttribute("exists", true);
					return "redirect:/respondent/add";
				}
			}
		}else{
			model.addAttribute("notValidUserKey", true);
			return "redirect:/respondent/add";
		}
		
		
		return "redirect:/respondent/all";
	}
	
	public static boolean isUserKeyValid(String userKey){
		return userKey.matches("^([A-Z0-9]{4})-([A-Z0-9]{4})-([A-Z0-9]{4})-([A-Z0-9]{4})");
	}
	
}
