package eu.slachta.pdp.model.server;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the testtype database table.
 * 
 */
@Entity
@Table(name="testtype")
@NamedQuery(name="Testtype.findAll", query="SELECT t FROM Testtype t")
public class Testtype implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int id;

	private String description;

	@Column(nullable=false, length=45)
	private String name;

	@Column(nullable=false, length=45)
	private String prefix;

	//bi-directional many-to-one association to Bourdontest
	@OneToMany(mappedBy="testtype")
	private List<Bourdontest> bourdontests;

	public Testtype() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrefix() {
		return this.prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public List<Bourdontest> getBourdontests() {
		return this.bourdontests;
	}

	public void setBourdontests(List<Bourdontest> bourdontests) {
		this.bourdontests = bourdontests;
	}

	public Bourdontest addBourdontest(Bourdontest bourdontest) {
		getBourdontests().add(bourdontest);
		bourdontest.setTesttype(this);

		return bourdontest;
	}

	public Bourdontest removeBourdontest(Bourdontest bourdontest) {
		getBourdontests().remove(bourdontest);
		bourdontest.setTesttype(null);

		return bourdontest;
	}

}